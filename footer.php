<?php /*
The template for displaying the footer
*/ ?>
						
	
<?php if ( is_page_template( 'template-landing.php' ) ) { ?>
  <footer id="footer" class="landing-footer">
    <div class="footer-contents max-width">
      <p><?php the_field('landing_legal_text'); ?></p>
      <img src="<?php bloginfo('stylesheet_directory'); ?>/img/afs-landing-logo.svg" alt="Applied Fitness Solutions" />
      <p>©Copyright <?php echo date('Y'); ?> Applied Fitness Solutions. | <a href="https://4afsfit.com">4afsfit.com</a></p>
    </div>
  </footer>


<?php } else { ?>
  <footer id="footer">
		<div class="footer-contents max-width">
      <div class="one-third">
        <h2>Ann Arbor <b>Hours</b></h2>
        <address><?php the_field('ann_arbor_address', 'option'); ?></address>
        <ul>
          <li><span>M</span>  <?php the_field('ann_arbor_monday', 'option'); ?></li>
          <li><span>TU</span>  <?php the_field('ann_arbor_tuesday', 'option'); ?></li>
          <li><span>W</span> <?php the_field('ann_arbor_wednesday', 'option'); ?></li>
          <li><span>TH</span>  <?php the_field('ann_arbor_thursday', 'option'); ?></li>
          <li><span>F</span> <?php the_field('ann_arbor_friday', 'option'); ?></li>
          <li><span>SA</span>  <?php the_field('ann_arbor_saturday', 'option'); ?></li>
          <li><span>SU</span>  <?php the_field('ann_arbor_sunday', 'option'); ?></li>
        </ul>
        <a href="tel:<?php the_field('ann_arbor_phone', 'option'); ?>" class="btn"><?php the_field('ann_arbor_phone', 'option'); ?></a>
        <a href="/contact-us/?location=Ann%20Arbor" class="btn">Contact Us</a>
      </div>
      <div class="one-third">
        <h2>Plymouth <b>Hours</b></h2>
        <address><?php the_field('plymouth_address', 'option'); ?></address>
        <ul>
          <li><span>M</span> <?php the_field('plymouth_monday', 'option'); ?></li>
          <li><span>TU</span>  <?php the_field('plymouth_tuesday', 'option'); ?></li>
          <li><span>W</span> <?php the_field('plymouthwednesday', 'option'); ?></li>
          <li><span>TH</span>  <?php the_field('plymouth_thursday', 'option'); ?></li>
          <li><span>F</span> <?php the_field('plymouth_friday', 'option'); ?></li>
          <li><span>SA</span>  <?php the_field('plymouth_saturday', 'option'); ?></li>
          <li><span>SU</span>  <?php the_field('plymouth_sunday', 'option'); ?></li>
        </ul>
        <a href="tel:<?php the_field('plymouth_phone', 'option'); ?>" class="btn"><?php the_field('plymouth_phone', 'option'); ?></a>
        <a href="/contact-us/?location=Plymouth" class="btn">Contact Us</a>
      </div>
      <div class="one-third">
        <h2>Rochester <b>Hours</b></h2>
        <address><?php the_field('rochester_address', 'option'); ?></address>
        <ul>
          <li><span>M</span> <?php the_field('rochester_monday', 'option'); ?></li>
          <li><span>TU</span>  <?php the_field('rochester_tuesday', 'option'); ?></li>
          <li><span>W</span> <?php the_field('rochesterwednesday', 'option'); ?></li>
          <li><span>TH</span>  <?php the_field('rochester_thursday', 'option'); ?></li>
          <li><span>F</span> <?php the_field('rochester_friday', 'option'); ?></li>
          <li><span>SA</span>  <?php the_field('rochester_saturday', 'option'); ?></li>
          <li><span>SU</span>  <?php the_field('rochester_sunday', 'option'); ?></li>
        </ul>
        <a href="tel:<?php the_field('rochester_phone', 'option'); ?>" class="btn"><?php the_field('rochester_phone', 'option'); ?></a>
        <a href="/contact-us/?location=Rochester" class="btn">Contact Us</a>
      </div>
      <div style="clear: both"></div>
    </div>
    <div class="bottom-bar">
      <div class="max-width">
        <div class="footer-menu">
          <?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?> 
          <div style="clear: both"></div>
        </div>
        <div id="copyright">©Copyright <?php echo date('Y'); ?> Applied Fitness Solutions. All Rights Reserved.</div> 
        <div class="footer-social">
          <a target="_blank" href="https://www.facebook.com/AppliedFitnessSolutions/" class="social-icon fb"></a>
          <a target="_blank" href="https://www.instagram.com/appliedfitnesssolutions/" class="social-icon insta"></a>
          <a target="_blank" href="https://www.youtube.com/user/AppliedFitSolutions" class="social-icon ytube"></a>
        </div> 
        <div style="clear: both"></div>
      </div>
    </div>
  </footer>
<?php } ?>

<!-- MOBILE NAVIGATION -->
	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/mobilenav.js"></script>
<!-- SMOOTH SCROLLING -->
	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/smoothscroll.js"></script>
<!-- ENTRANCE ANIMATIONS -->
	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/entranceanimation.js"></script>
	<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.viewportchecker.min.js"></script>
<!-- Sticky Elements -->
  <script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.sticky-kit.min.js"></script>
  <script type="text/javascript">
    jQuery(document).ready(function(){
      jQuery("#sticky").stick_in_parent({ parent: 'body' });
    });
  </script>
<!-- SEARCH BAR APPEAR -->
  <script type="text/javascript">
    jQuery('#search-icon').on('click',function(){
      jQuery('.search-bar').addClass('active');
    });
    jQuery('#close-bar').on('click',function(){
      jQuery('.search-bar').removeClass('active');
    });
  </script>
<!-- LIGHTBOX -->
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.fancybox.js"></script>
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.fancybox-media.js"></script>
  <script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('.fancybox').fancybox({
          type: "iframe",
          showCloseButton: true,
          autoSize : false,
          width    : "100%",
          height   : "100%",
          afterShow: function () {
            $(".non-merci").on("click", function () {
                $.fancybox.close()
            });
          }
        });
    });
  </script>
<!-- ARC TEXT -->
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.arctext.js"></script>
  <script>
    var $arc = jQuery('#arc-text').hide();
    $arc.show().arctext({radius: 600});
  </script>
<!-- ACCORDIAN ICONS -->
  <script>
    jQuery('.archive-accordion-year').click(function(){
      if ( jQuery(this).hasClass('open') ) {
        jQuery('.archive-accordion-year').removeClass('open');
        jQuery(this).removeClass('open');
      } else {
        jQuery('.archive-accordion-year').removeClass('open');
        jQuery(this).addClass('open');
      }
    });
  </script>
<!-- PARALAX -->
  <script type="text/javascript">
    jQuery(window).scroll(function () {
        jQuery(".always-with-you .paralax-bg").css("background-position","right " + (jQuery(this).scrollTop() / 25) + "px");
    });
  </script>
  <script type="text/javascript">
    jQuery(window).scroll(function () {
        jQuery(".endless-support .paralax-bg").css("background-position","left " + (jQuery(this).scrollTop() / 25) + "px");
    });
  </script>
<!-- FLEX SLIDER -->
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.flexslider-min.js"></script>
  <script>
    jQuery(window).load(function() {
      jQuery('.flexslider').flexslider({
        animation: "slide",
        animationLoop: true,
        itemWidth: 210,
        itemMargin: 0,
        move: 1,
        minItems: 3,
        maxItems: 3,
        slideshow: true,
        slideshowSpeed: 4000,
        pauseOnAction: true,
        pauseOnHover: false,
        touch: true,
				end : function(slider){
					jQuery('.flexslider .slides li').each(function(){
						slider.addSlide('<li>'+jQuery(this).context.innerHTML+'</li>', slider.count);
						jQuery('.flexslider .slides').append('<li>'+jQuery(this).context.innerHTML+'</li>');
					});
				}
      });
    });
  </script>
<!-- MASONARY GRID & JAX LOADING FOR BLOG -->
  <script src="<?php bloginfo('stylesheet_directory'); ?>/js/masonry.pkgd.min.js"></script> 
  <script src="<?php bloginfo('stylesheet_directory'); ?>/js/imagesloaded.pkgd.js"></script> 
  <script> 
    jQuery(function() {
      var masonryInit = true; // set Masonry init flag
      jQuery.fn.almComplete = function(alm){ // Ajax Load More callback function
        if(jQuery('.grid').length){
          var $container = jQuery('.grid ul'); // our container
          if(masonryInit){
            // initialize Masonry only once
            masonryInit = false;
            $container.masonry({
              itemSelector: '.grid-item'
            });         
          }else{
              $container.masonry('reloadItems'); // Reload masonry items after callback
          }
          $container.imagesLoaded( function() { // When images are loaded, fire masonry again.
            $container.masonry();
          });
        }
      };
    });
  </script> 
  <script>
    jQuery('.search-grid').masonry({
      itemSelector: '.grid-item',
    });
  </script>
<!-- DIGITAL CONSULTATION -->
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/digital-consultation.js"></script>
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/digital-results.js"></script>
	
</body>

<?php wp_footer(); ?>

</html>