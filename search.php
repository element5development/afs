<?php /*
The template for displaying the container for search results pages
*/ ?>

<?php get_header(); ?>

<main class="full-width">

	<!-- PAGE TITLES -->
	<?php get_template_part( 'template-parts/content', 'page-top' ); ?>

	<section class="search-feed max-width">
		<?php $search_query = get_search_query(); ?>
		<!-- Loop Start -->
		<?php if ( have_posts() ) : ?>
			<div class="search-grid">
				<?php while ( have_posts() ) : the_post();
					get_template_part( 'template-parts/content', 'search' );
				endwhile; ?>
			</div>
			<?php the_posts_pagination( array(
					'prev_text'          => __( '<', 'twentysixteen' ),
					'next_text'          => __( '>', 'twentysixteen' ),
					'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( '', 'twentysixteen' ) . ' </span>',
				) ); ?>
		<?php else : ?>
			
			<?php get_template_part( 'template-parts/content', 'nothing-found' ); ?>

		<?php endif; ?>
		<!-- Loop End -->


	</section>
</main>

<?php get_footer(); ?>