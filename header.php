<?php /*
The Header template for our theme
*/ ?>

<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta name="instapage-verification" content="1527704508470-IC37" />
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
<!-- Favicon -->
	<link rel="shortcut icon" type="image/png" href="<?php bloginfo('stylesheet_directory'); ?>/img/favicon.png" />
<!-- Mobile Site Media Query -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Main CSS -->
	<link href="<?php bloginfo('stylesheet_url');?>" rel="stylesheet" type="text/css" />
<!-- Entrance Animations -->
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/animate.css" />
<!-- LightBox -->
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/jquery.fancybox.css" />
<!-- Flex Slider -->
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/flexslider.css" />
<!-- Facebook Pixel Code --> 
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod? 
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n; 
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0; 
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window, 
document,'script','https://connect.facebook.net/en_US/fbevents.js'); 
fbq('init', '604157886456070'); // Insert your pixel ID here. 
fbq('track', 'PageView'); 
</script>
<noscript><﻿img height="1" width="1" style="display:none" 
src="https://www.facebook.com/tr?id=604157886456070&ev=PageView&noscript=1" 
/></noscript> 
<!-- DO NOT MODIFY --> 
<!-- End Facebook Pixel Code --> 
<!-- Facebook Pixel Code --> 
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod? 
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n; 
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0; 
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window, 
document,'script','https://connect.facebook.net/en_US/fbevents.js'); 
fbq('init', '790894067743034'); // Insert your pixel ID here. 
fbq('track', 'PageView'); 
</script>
<noscript><﻿img height="1" width="1" style="display:none" 
src="https://www.facebook.com/tr?id=790894067743034&ev=PageView&noscript=1" 
/></noscript> 
<!-- DO NOT MODIFY --> 
<!-- End Facebook Pixel Code --> 
</head>

<body <?php body_class(); ?> >

  <!-- PIXEL -->
  <?php if ( is_front_page() || is_home() || is_singular() || is_page(37) || is_page(229) ) { ?>
    <script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=1128104&mt_adid=181048&v1=&v2=&v3=&s1=&s2=&s3='></script>
  <?php } ?>

  <!-- Facebook Pixel Code --> 
	<?php if ( is_page(223) || is_page(11253) ) { ?>
    <script> fbq('track', 'Lead'); </script>
    <﻿<script> fbq('track', 'Lead'); </script>
  <?php } ?>
  <!-- End Facebook Pixel Code --> 

	<!-- Main Navigation Bar Displayed On All Monitors And Tabets -->
	<header id="sticky" class="main-nav full-width">
		<div class="max-width">
			<a href="/">
				<img id="nav-logo" src="<?php bloginfo('stylesheet_directory'); ?>/img/AFS-Logo-color.svg" alt="Applied Fitness Solutions" />
				<img id="sticky-nav-logo" src="<?php bloginfo('stylesheet_directory'); ?>/img/AFS-mini-Logo-color.png" alt="Applied Fitness Solutions" />
			</a>
			<div class="header-right">
				<div class="top-bar">
		          <a href="tel:+18774237348" class="one-third">
		            <img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-phone-white.png" />
		            877-4AFS-FIT
		          </a>
					<a href="/schedule/" class="one-third">
						<img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-calendar.png" alt="AFS schedule"/>
						schedule
					</a>
					<a href="https://cms.4afsfit.com/user/login" class="one-third">
						<img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-login.png" alt="AFS client login"/>
						client login
					</a>
					<div style="clear: both"></div>
				</div>
				<div class="middle-menu">
					<div id="search-icon"></div>
					<div class="locations-careers">
						<a href="/contact-us/">Contact</a> | <a href="/locations/">Locations</a> | <a href="/afs-careers/">Careers</a> 
					</div>
				</div>
				<nav>
					<?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
				</nav>
			</div> 
			<div style="clear: both"></div>
		</div>

		<!--SEARCH BAR APPEAR-->
		<div class="search-bar">
			<?php get_search_form(); ?>
			<div id="close-bar"></div>
		</div>

	</header>

	<!--MOBILE MENU-->
	<header class="mobile-menu full-width">
		<div class="max-width">
			<div class="mobile-call">
				<a href="tel:+18774237348">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-phone-blue.png" alt="" />
					Call
				</a>
		  </div> 
			<a href="/">
				<img id="mobile-logo" src="<?php bloginfo('stylesheet_directory'); ?>/img/AFS-mini-Logo-color.png" alt="Applied Fitness Solutions" />
			</a>
			<div id="mobile-menu-icon" class="mobile-menu-icon">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-mobile-menu.svg" alt="AFS Menu"/>
				Menu
			</div>
			<div style="clear: both"></div>
		</div>

	</header>

	<div id="menu-open" class="menu-open">
		<img id="menu-close" src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-mobile-close.svg" alt="AFS Close Menu"/>
		<a href="/schedule/" class="links schedule">
			<img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-calendar.png" alt="AFS Schedule"/>
			schedule
		</a>
		<a href="https://cms.4afsfit.com/user/login" class="links login">
			<img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-login.png" alt="AFS client login"/>
			client login
		</a>
		<nav>
			<?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
		</nav>
		<div class="social-share">
			<a target="_blank" href="https://www.facebook.com/AppliedFitnessSolutions/" class="social-icon fb"></a>
			<a target="_blank" href="https://www.instagram.com/appliedfitnesssolutions/" class="social-icon insta"></a>
			<a target="_blank" href="https://www.youtube.com/user/AppliedFitSolutions" class="social-icon ytube"></a>
		</div>
		<div class="locations-careers">
			<a href="/contact-us/">Contact</a> | <a href="/locations/">Locations</a> | <a href="/afs-careers/">Careers</a> 
		</div>
	</div>