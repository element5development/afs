<?php /*
The template for displaying all single posts and attachments
*/ ?>

<?php get_header(); ?>

<?php 
	$designation = strip_tags(get_the_term_list(get_the_ID(), $TLPteampro->taxonomies['designation'], null, ','));
	$department = strip_tags(get_the_term_list(get_the_ID(), $TLPteampro->taxonomies['department'], null, ','));

	$titles = get_field('titles');
	$alltitles = null;
	if( $titles ) {
	  foreach( $titles as $singletitle ) {
	    $alltitles .= '<span>, </span>'.$singletitle.'';
	  }
	}
?>

<main class="full-width">

	<!-- PAGE TITLES -->
	<?php get_template_part( 'template-parts/content', 'page-top' ); ?>

	<div class="max-width clearfix">

		<section class="single-post content-left clearfix">
			<?php the_content(); ?>
		</section>

		<aside class="sidebar sidebar-right">
			<div class="widget trainer-widget">
				<?php the_post_thumbnail(); ?>
				<h3><?php the_title(); ?></h3>
				<?php if( $department ) { ?>
        	<h4 class="tlp-department"><?php echo $department ?></h4>
        <?php } ?>
				<?php if( $designation ) { ?>
        	<h4 class="tlp-position"><?php echo $designation ?></h4>
        <?php } ?>
        <?php if( $alltitles ) { ?>
        	<div class="team-titles"><?php echo $alltitles ?></div>
        <?php } ?>
			</div>
		</aside>

		<div style="clear: both"></div>
	</div>

	<?php 
		$login = get_field('author');
		$user = get_userdatabylogin($login);
		$user_id = $user->ID; 

	if ( user_has_posts($user_id) ) { ?>

		<section class="success-stories-cta full-width">
			<div class="above-overlay">
				<div class="max-width">
					<h2><strong>Success</strong> Stories </h2>
					<a href="/category/success-stories/" class="secondary-button arrow">View More</a>
				</div>
			</div>
			<div class="dark-overlay"></div>
	</section>

	<?php } else { 
		// NOTHING
	} ?>

</main>

<?php get_footer(); ?>