<?php /*
Template Name: Landing Page
*/ ?>

<?php get_header(); ?>

<main class="full-width">

	<!-- PAGE TITLES -->
	<?php get_template_part( 'template-parts/content', 'page-top' ); ?>

  <!-- LANDING FORM -->
  <?php if ( get_field('landing_form') ) { 
	 get_template_part( 'template-parts/content', 'landing-form' ); 
   } ?>

  <!-- LANDING VIDEO -->
  <?php if ( get_field('landing_video') ) { 
    get_template_part( 'template-parts/content', 'landing-video' ); 
  } ?>

  <!-- PAGE CONTENT -->
  <?php if ( get_the_content() ) : ?>
    <section class="page-contents landing-page-contents max-width">
      <div class="one-third">
        <img src="<?php the_field('success_story_before'); ?>" />
        <div id="before-label" class="img-labels">before</div>
        <img src="<?php the_field('success_story_After'); ?>" />
        <div id="after-label"  class="img-labels">after</div>
      </div>
      <div class="two-third">
        <div class="label">Success Story</div>
        <?php the_content(); ?>
      </div>
    </section>
  <?php endif; ?>
<!-- LOCATION DETAILS -->

<section class="page-contents landing-page-contents max-width">
      <h2><?php the_field('location_title')?></h2>
      <div class="one-third">
      <?php the_field('google_map_iframe') ?>
      </div>
    <div class="two-third">
      <?php the_field('location_address') ?>
      <?php the_field('location_phone') ?>
      <?php the_field('google_map_link') ?>
      <?php the_field('location_features') ?>
  </div>
</section>

  <!-- LANDING CTA -->
  <?php if ( get_field('landing_cta') ) { 
    get_template_part( 'template-parts/content', 'landing-cta' ); 
  } ?>


</main>

<?php get_footer(); ?>