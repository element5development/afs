<?php /*
AUTHOR POSTS
*/ ?>

<?php get_header(); ?>

<main class="full-width">

	<!-- PAGE TITLES -->
	<?php get_template_part( 'template-parts/content', 'page-top' ); ?>

	<div class="max-width clearfix">

		<?php  
			$author = get_user_by( 'slug', get_query_var( 'author_name' ) );
			$author_id = $author->ID;
		?>

		<h2>Archive: <?php echo $author->first_name . ' ' . $author->last_name; ?></h2>

		<section class="grid blog content-left clearfix">
			<?php echo do_shortcode('[ajax_load_more category="success-stories" author="' . $author_id . '" posts_per_page="9" scroll="false" transition_container="false" button_label="Load More"]'); ?>
		</section>

		<!--SIDEBAR-->
		<?php get_template_part( 'template-parts/content', 'right-sidebar' ); ?>

	</div>
</main>

<?php get_footer(); ?>