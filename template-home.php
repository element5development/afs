<?php /*
Template Name: Home Page
*/ ?>

<?php get_header(); ?>

<main class="full-width">

	<!-- PAGE TITLES -->
	<?php get_template_part( 'template-parts/content', 'page-top' ); ?>

  <!-- SUCESS STORIES PREVIEW -->
	<?php get_template_part( 'template-parts/content', 'success-stories-preview' ); ?>

  <!-- TEAM SLIDER -->
<!--   <section class="team-slider max-width">
    <h2>Meet our <b>Team</b></h2>
    <?php echo do_shortcode( '[tlpteam id="79" title="Home Slider"]' ) ?>
  </section> -->

  <!-- CONSULTATION CTA -->
  <?php get_template_part( 'template-parts/content', 'where-to-start' ); ?>

  <!-- BLOG PREVIEW -->
  <?php get_template_part( 'template-parts/content', 'blog-preview' ); ?>

</main>

<?php get_footer(); ?>