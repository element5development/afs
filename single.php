<?php /*
The template for displaying all single posts and attachments
*/ ?>

<?php get_header(); ?>

<main class="full-width">

	<!-- PAGE TITLES -->
	<?php get_template_part( 'template-parts/content', 'page-top' ); ?>

	<div class="max-width clearfix">

		<?php if ( in_category('success-stories') ) { ?>
		<!--SUCESS STORIES-->
			<section class="single-post sucess-story-post content-left clearfix">
				<?php the_content(); ?>

				<div class="story-stats">
					<h3><?php the_field('first_name'); ?>'s Stats</h3>
					<?php if( have_rows('stats') ) {
				    while ( have_rows('stats') ) : the_row(); ?>
				    		
				        <div class="one-third">
					        <?php if ( get_sub_field('stat') == "Weight" ) { ?>
					        	<h4>Weight</h4>
					        	<div class="img-bg">
					        		<img id="weight-one" src="<?php bloginfo('stylesheet_directory'); ?>/img/weight-one.png" />
					        		<img id="weight-two" src="<?php bloginfo('stylesheet_directory'); ?>/img/weight-two.png" />
					        	</div>
					        <?php } elseif ( get_sub_field('stat') == "Body Fat Percentage" ) { ?>
					        	<h4>Body Fat Percentage</h4>
					        	<div class="img-bg">
					        		<img id="body-fat-one" src="<?php bloginfo('stylesheet_directory'); ?>/img/body-fat-one.png" />
					        		<img id="body-fat-two" src="<?php bloginfo('stylesheet_directory'); ?>/img/body-fat-two.png" />
					        	</div>
					        <?php } elseif ( get_sub_field('stat') == "Lean Mass" ) { ?>
					        	<h4>Lean Mass</h4>
					        	<div class="img-bg">
					        		<img id="lean-one" src="<?php bloginfo('stylesheet_directory'); ?>/img/lean-one.png" />
					        		<img id="lean-two" src="<?php bloginfo('stylesheet_directory'); ?>/img/lean-two.png" />
					        	</div>
					        <?php } elseif ( get_sub_field('stat') == "Fat Mass" ) { ?>
					        	<h4>Fat Mass</h4>
					        	<div class="img-bg">
					        		<div class="center-circle"></div>
					        		<img id="fat-one" src="<?php bloginfo('stylesheet_directory'); ?>/img/fat-one.svg" />
					        		<img id="fat-two" src="<?php bloginfo('stylesheet_directory'); ?>/img/fat-two.svg" />
					        	</div>
					        <?php } elseif ( get_sub_field('stat') == "Measurements" ) { ?>
					        	<h4>Measurements</h4>
					        	<div class="img-bg">
					        		<img id="measurement-one" src="<?php bloginfo('stylesheet_directory'); ?>/img/measurments-one.png" />
					        		<img id="measurement-two" src="<?php bloginfo('stylesheet_directory'); ?>/img/measurments-two.png" />
					        	</div>
					        <?php } else { ?>
					        	<h4>Heart Rate</h4>
					        	<div class="img-bg">
					        		<img id="heart-rate" src="<?php bloginfo('stylesheet_directory'); ?>/img/heart-one.png" />
					        	</div>
					        <?php } ?>
				        	<p><?php the_sub_field('details'); ?></p>
				        </div>
				    <?php endwhile;
					} ?>
					<div style="clear: both;"></div>
					<?php if ( get_field('accomplishments') ) { ?>
						<div class="accomplishments">
							<div class="img-bg">
		        		<img id="accomplishment-one" src="<?php bloginfo('stylesheet_directory'); ?>/img/accomplishment-one.png" />
		        		<img id="accomplishment-two" src="<?php bloginfo('stylesheet_directory'); ?>/img/accomplishment-two.png" />
		        		<img id="accomplishment-three" src="<?php bloginfo('stylesheet_directory'); ?>/img/accomplishment-three.png" />
		        	</div>
							<div class="accomplishment-details">
								<h4>accomplishments</h4>
								<p><?php the_field('accomplishments'); ?></p>
							</div>
							<div style="clear: both;"></div>
						</div>
					<?php } ?>
				</div>
				<div class="trainer-quote">
					<div class="quote-contents">
						<h4>I'm <?php the_field('first_name'); ?>'s Trainer, <?php the_field('trainer_name'); ?></h4>
						<p><?php the_field('trainer_quote'); ?></p>
					</div>
					<img src="<?php the_field('trainer_headshot'); ?>" />
					<div style="clear: both;"></div>
				</div>
				<?php if ( get_field('closing_text') ) { ?>
					<div class="closing-thoughts">
						<div class="person-today" style="background-image: url(<?php the_field('person_today'); ?>);"></div>
						<div class="closing-contents">
							<h3><?php the_field('first_name'); ?> Today</h3>
							<p><?php the_field('closing_text'); ?></p>
							<?php if ( get_field('program_cta') == 'Weight Loss' ) { ?>
								<a href="/our-classes/weight-loss-solution/" class="primary-button">Want similar results?</a>
							<?php } elseif ( get_field('program_cta') == 'Strength Training' ) { ?>
								<a href="/our-classes/strength-solution/" class="primary-button">Want similar results?</a>
							<?php } elseif ( get_field('program_cta') == 'Fitness Solutions' ) { ?>
								<a href="/our-classes/fitness-solution/" class="primary-button">Want similar results?</a>
							<?php } elseif ( get_field('program_cta') == 'Athlete Solution' ) { ?>
								<a href="/our-classes/athletes/" class="primary-button">Want similar results?</a>
							<?php } elseif ( get_field('program_cta') == 'Performance Solutions' ) { ?>
								<a href="/our-classes/performance-solution/" class="primary-button">Want similar results?</a>
							<?php } elseif ( get_field('program_cta') == 'Mobility Solution' ) { ?>
								<a href="/our-classes/mobility-solution/" class="primary-button">Want similar results?</a>
							<?php } else { } ?>
						</div>
						<div style="clear: both;"></div>
					</div>
				<?php } ?>
				<?php if ( get_field('contact_form') ) {
					the_field('contact_form');
				} ?>
			</section>

		<?php } else { ?>
		<!--ALL OTHER POSTS-->
			<section class="single-post content-left clearfix">
				<?php the_content(); ?>
			</section>
		<?php } ?>

		<aside class="sidebar sidebar-right">
			<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('sidebar')) : else : ?>
				<p><strong>Widget Ready</strong></p>  
			<?php endif; ?> 
			<?php if ( in_category('success-stories') ) { ?>
				<a href="/category/success-stories/"><div class="widget stories-widget">
					<h3>View more<br/><strong>Success Stories</strong></h3>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-arrow-white.svg" />
				</div></a>
			<?php } ?>
			<div class="widget consultation-widget">
				<h3>Get a free<br/><strong>In-Person<br/>Consultation</strong></h3>
				<p>We have one goal—to get you in the best shape of your life! Whether “in shape” means losing 10 lbs to 150 lbs or setting a new marathon PR, AFS has the systems and the science that will get you there.</p>
				<a href="/" class="secondary-button arrow">Get Yours</a>
			</div>
			<div class="widget newsletter-widget">
				<h3>Sign up for<br/><strong>our Updates</strong></h3>
				<p>Stay up to date with us by subscribing to our newsletter.</p>
					<?php echo do_shortcode('[constantcontactapi formid="1"]'); ?>
			</div>
		</aside>

	</div>

</main>

<?php get_footer(); ?>