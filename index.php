<?php /*
MAIN BLOG
*/ ?>

<?php get_header(); ?>

<main class="full-width">

	<!-- PAGE TITLES -->
	<?php get_template_part( 'template-parts/content', 'page-top' ); ?>

	<div class="max-width clearfix">

		<div class="blog content-left clearfix">
			<div class="grid">
				<?php echo do_shortcode('[ajax_load_more post_type="post" posts_per_page="9" scroll="false" transition_container="false" button_label="Load More"]'); ?>
			</div>
		</div>

		<!--SIDEBAR-->
		<?php get_template_part( 'template-parts/content', 'right-sidebar' ); ?>

	</div>
</main>

<?php get_footer(); ?>