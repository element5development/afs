/* Entrance Animations */

/*TIMELINE POSTS BOUNCE INTO SCREEN FROM RIGHT*/
jQuery(document).ready(function() {
  jQuery('.step-container').addClass("hidden").viewportChecker({
    classToAdd: 'visible animated zoomIn',
    offset: 300
  });
  jQuery('.program-container').addClass("hidden").viewportChecker({
    classToAdd: 'visible animated fadeInUp',
    offset: 300
  });
  jQuery('.include-item').viewportChecker({
    classToAdd: 'visible animated pulse',
    offset: 400
  });
  jQuery('.table-container').addClass("hidden").viewportChecker({
    classToAdd: 'visible animated zoomIn',
    offset: 300
  });
  jQuery('.mission').addClass("hidden").viewportChecker({
    classToAdd: 'visible animated fadeInUp',
    offset: 300
  });
  jQuery('.locations-container').addClass("hidden").viewportChecker({
    classToAdd: 'visible animated zoomIn',
    offset: 300
  });
  jQuery('.career-intro-entrance').addClass("hidden").viewportChecker({
    classToAdd: 'visible animated fadeInUp',
    offset: 300
  });
  jQuery('.childcare-price').addClass("hidden").viewportChecker({
    classToAdd: 'visible animated zoomIn',
    offset: 300
  });
  jQuery('.childcare-hours').addClass("hidden").viewportChecker({
    classToAdd: 'visible animated fadeInUp',
    offset: 300
  });
  jQuery('.weight-loss-pricing').addClass("hidden").viewportChecker({
    classToAdd: 'visible animated zoomIn',
    offset: 300
  });
  jQuery('.value-tile').addClass("hidden").viewportChecker({
    classToAdd: 'visible animated zoomIn',
    offset: 300
  });
});

