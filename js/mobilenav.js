/* Mobile Navigation Open and Close */

jQuery(document).ready(function() {
  jQuery("#mobile-menu-icon").click(function(e) {
    if(jQuery("#menu-open").hasClass("open")) {
      jQuery("#menu-open").removeClass("open");
    } else {
      jQuery("#menu-open").addClass("open");
    }
   });
  jQuery("#menu-close").click(function(e) {
    jQuery("#menu-open").removeClass("open");
  });
});

jQuery(document).ready(function() {
  jQuery("footer .one-third").click(function(e) {
    if(jQuery(this).hasClass("open")) {
      jQuery(this).removeClass("open");
    } else {
      jQuery(this).addClass("open");
    }
   });
});