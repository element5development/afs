<?php /*
DEFAULT PAGE TEMPLATE
*/ ?>

<?php get_header(); ?>

<main class="full-width">

	<!-- PAGE TITLES -->
	<?php get_template_part( 'template-parts/content', 'page-top' ); ?>

  <!-- SECONDARY NAV -->
  <?php get_template_part( 'template-parts/content', 'secondary-nav' ); ?>

  <!-- PAGE INTRO -->
  <?php if ( get_field('introduction_text') ) { ?>
    <?php get_template_part( 'template-parts/content', 'page-intro' ); ?>
  <?php } ?>

  <!-- CUSTOMIZED WORKOUTS -->
  <?php if ( is_page(229) ) { ?>
    <?php get_template_part( 'template-parts/content', 'customized-workouts' ); ?>
  <?php } ?>

  <!-- COMPARING AFS TO BIG GYMS -->
  <?php if ( get_field('big_box_gym_list') && get_field('afs_list') ) { ?>
    <?php get_template_part( 'template-parts/content', 'afs-difference' ); ?>
  <?php } ?>

  <!-- MEMBERSHIPS INCLUDE LIST -->
  <?php if ( have_rows('include') ) { ?>
    <?php get_template_part( 'template-parts/content', 'membership-include' ); ?>
  <?php } ?>

  <!-- CAREERS INTRO -->
  <?php if ( is_page(53) ) { ?>
    <?php get_template_part( 'template-parts/content', 'career-intro' ); ?>
  <?php } ?>

  <!-- CHILDCARE SERVICES -->
  <?php if ( get_field('childcare_services_content') ) { ?>
    <?php get_template_part( 'template-parts/content', 'childcare-services' ); ?>
  <?php } ?>

  <!-- HISTORY & MISSION -->
  <?php if ( get_field('history_text') && have_rows('mission_point') ) { ?>
    <?php get_template_part( 'template-parts/content', 'history-mission' ); ?>
  <?php } ?>

  <!-- PROGRAM INTRO -->
  <?php if ( get_field('intro_content') ) { ?>
    <?php get_template_part( 'template-parts/content', 'weight-loss-intro' ); ?>
  <?php } ?>

  <!-- LOCATION DETAILS -->
  <?php if ( get_field('location_title') && get_field('location_features') ) { ?>
    <?php get_template_part( 'template-parts/content', 'location-details' ); ?>
  <?php } ?>

  <!-- FULLSCREEN GALLERY -->
  <?php if ( get_field('gallery') ) { ?>
    <?php if ( is_page(12325) ) { ?>
      <h2 class="header-bar program-gallery"><div id="program-gallery"></div>Training <b>Gallery</b></h2>
    <?php } elseif ( $post->post_parent == 229 ) { ?>
      <h2 class="header-bar program-gallery"><div id="program-gallery"></div><?php the_title(); ?> <b>Gallery</b></h2>
    <?php } ?>
    <?php get_template_part( 'template-parts/content', 'gallery' ); ?>
  <?php } ?>

  <!-- DEFAULT CONTENT SECTION -->
  <?php if ( is_page(12325) ) { ?>
    <h2 class="header-bar program-difference"><div id="program-difference"></div>Why Medical Professionals <b>Trust AFS</b></h2>
  <?php } elseif (  $post->post_parent == 229 ) { ?>
    <h2 class="header-bar program-difference"><div id="program-difference"></div>Why this <?php the_title(); ?> Program <b>is Different</b></h2>
  <?php } ?>
	<?php if ( get_the_content() ) : ?>
		<section class="page-contents max-width">
			<?php the_content(); ?>
		</section>
	<?php endif; ?>

  <!-- CHARITY CHALLENGE -->
  <?php if ( get_field('charity_challenge_cta') ) { ?>
    <?php get_template_part( 'template-parts/content', 'charity' ); ?>
  <?php } ?>

  <!-- DIGITAL CONSULTATION -->
  <?php if ( is_page(11220) ) { ?>
    <?php get_template_part( 'template-parts/content', 'digital-consultation' ); ?>
  <?php } ?>

  <!-- DIGITAL CONSULTATION RESULTS -->
  <?php if ( is_page(11253) ) { ?>
    <?php get_template_part( 'template-parts/content', 'digital-results' ); ?>
  <?php } ?>

  <!-- PRORGAM SECONDARY NAV-->
  <?php if ( have_rows('program_navigation') ) { ?>
    <?php get_template_part( 'template-parts/content', 'program-nav' ); ?>
  <?php } ?>

  <!-- CHOOSE LOCATION HEADER -->
  <?php if ( is_page(10993) ) { ?>
    <?php get_template_part( 'template-parts/content', 'all-trainers' ); ?>
  <?php } ?>

  <!-- CURRENT OPENINGS -->
  <?php if ( have_rows('openings') ) { ?>
    <a id="opening-link"></a>
    <?php get_template_part( 'template-parts/content', 'openings' ); ?>
  <?php } if ( !have_rows('openings') && is_page(53) ) { ?>
    <a id="opening-link"></a>
    <?php get_template_part( 'template-parts/content', 'openings-none' ); ?>
  <?php } ?>

  <!-- LOCATIONS -->
  <?php if ( have_rows('locations') ) { ?>
    <?php get_template_part( 'template-parts/content', 'locations' ); ?>
  <?php } ?>

  <!-- PROGRAM FAQ -->
  <?php if ( get_field('afs_memberships') ) { ?>
    <?php get_template_part( 'template-parts/content', 'program-faq' ); ?>
  <?php } ?>

  <!-- CONSULTATION CTA *BUTTONS ONLY* -->
  <?php if ( is_page(38) ) { ?>
    <?php get_template_part( 'template-parts/content', 'where-to-start-simplified' ); ?>
  <?php } ?>

  <!-- COACH ALWAYS WITH YOU -->
  <?php if ( is_page(37) ) { ?>
    <?php get_template_part( 'template-parts/content', 'always-with-you' ); ?>
  <?php } ?>

  <!-- PRICE TABLES -->
  <?php if ( have_rows('price_table') ) { ?>
    <?php get_template_part( 'template-parts/content', 'price-tables' ); ?>
  <?php } ?>

  <!--  GETING STARTED STEPS-->
  <?php if ( get_field('steps') ) { ?>
    <?php get_template_part( 'template-parts/content', 'getting-started' ); ?>
  <?php } ?>

  <!--  PROGRAM SOLUTIONS PREVIEW -->
  <?php if ( get_field('program') ) { ?>
    <?php get_template_part( 'template-parts/content', 'program-ctas' ); ?>
  <?php } ?>

  <!-- INSTAGRAM GALLERY -->
  <?php if ( is_page(53) || is_page(11151) || is_page(11150) || is_page(11152) ) { ?>
    <?php get_template_part( 'template-parts/content', 'instagram' ); ?>
  <?php } ?>

  <!-- CHILDCARE HOURS -->
  <?php if ( get_field('ann_arbor_monday') ) { ?>
    <?php get_template_part( 'template-parts/content', 'childcare-hours' ); ?>
  <?php } ?>

  <!-- IMPORTANT LINKS -->
  <?php // if ( is_page(10992) ) { ?>
    <?php //get_template_part( 'template-parts/content', 'important-links' ); ?>
  <?php // } ?>

  <!-- INHOUOSE CHILDCARE -->
  <?php if ( is_page(39) || is_page(229) || $post->post_parent == '39' || $post->post_parent == '229' ) { ?>
    <?php if ( !is_page(10994) ) { ?>
      <?php get_template_part( 'template-parts/content', 'childcare' ); ?>
    <?php } ?>
  <?php } ?>

  <!-- CONSULTATION CTA -->
  <?php if ( !is_front_page() && !is_page(227) && !is_page(53) && !is_page(38) ) { ?>
    <?php get_template_part( 'template-parts/content', 'where-to-start' ); ?>
  <?php } ?>

</main>

<?php get_footer(); ?>