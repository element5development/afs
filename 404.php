<?php /*
The template for displaying 404 pages (broken links)
*/ ?>

<?php get_header(); ?>

<main class="full-width">

  <!-- PAGE TITLES -->
  <?php get_template_part( 'template-parts/content', 'page-top' ); ?>

  <section class="page-contents error-page max-width">
    <h2><b>404 ERROR</b></h2>
    <h3>It seems you have found a lost page.</h3>
    <h4>You might have better luck if you try searching for what you want.</h4>
   <?php get_search_form(); ?>

  </section>

</main>

<?php get_footer(); ?>
