<?php /*
ARCHIEVE & CATEGORY PAGE FOR DEFAULT POST TYPE
*/ ?>

<?php get_header(); ?>

<main class="full-width">

	<!-- PAGE TITLES -->
	<?php get_template_part( 'template-parts/content', 'page-top' ); ?>

	<div class="max-width clearfix">

		<?php  
			$year = get_query_var('year');
			$month = get_query_var('monthnum');
		?>

		<h2>Archive: <?php echo $month ?>/<?php echo $year ?></h2>

		<section class="grid blog content-left clearfix">
			<?php echo do_shortcode('[ajax_load_more container_type="ul" post_type="post" year=" ' . $year . ' " month=" ' . $month . ' " posts_per_page="9" scroll="false" transition_container="false" button_label="Load More"]'); ?>
			<div style="clear: both"></div>
		</section>

		<!--SIDEBAR-->
		<?php get_template_part( 'template-parts/content', 'right-sidebar' ); ?>

	</div>
</main>

<?php get_footer(); ?>