<?php

//SETUP WORDPRESS & BACKEND
    // REMOVE PARENT WIDGET AREAS
        function remove_parent_theme_widgets(){
            unregister_sidebar( 'sidebar-1' );
            unregister_sidebar( 'sidebar-2' );
            unregister_sidebar( 'sidebar-3' );
        }
        add_action( 'widgets_init', 'remove_parent_theme_widgets', 11 );
    // REMOVE PARENT THEME MENUS
        function remove_default_menu(){
            unregister_nav_menu('primary');
            unregister_nav_menu('social');
        }
        add_action( 'after_setup_theme', 'remove_default_menu', 11 );
    // ADD JQUERY TO WORDPRESS
        if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
        function my_jquery_enqueue() {
           wp_deregister_script('jquery');
           wp_register_script('jquery', "https" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js", false, null);
           wp_enqueue_script('jquery');
        }
    // ADD MENUS FOR NAVIGATION
        function register_menu () {
            register_nav_menu('main-menu', __('Header Menu'));
            register_nav_menu('weight-loss-menu', __('Weight Loss Menu'));
            register_nav_menu('footer-menu', __('Footer Menu'));
        }
        add_action('init', 'register_menu');
    // ADD CLASSES TO MENU LINKS
        function add_menuclass($ulclass) {
            return preg_replace('/<a rel="smoothScroll"/', '<a rel="smoothScroll" class="smoothScroll"', $ulclass, 3);
            }
        add_filter('wp_nav_menu','add_menuclass');
    // ADD ACF OPTIONS PAGE
        if( function_exists('acf_add_options_page') ) {
            acf_add_options_page();   
        }
    // ALLOW SVG FILES IN WORDPRESS
        function cc_mime_types($mimes) {
          $mimes['svg'] = 'image/svg+xml';
          return $mimes;
        }
        add_filter('upload_mimes', 'cc_mime_types');
    // ALLOW EDITOR ACCESS TO GRAVITY FORMS, MENUS & WIDGETS
        function add_grav_forms(){
            $role = get_role('editor');
            $role->add_cap('gform_full_access');
            $role->add_cap('edit_theme_options');
            $role->add_cap('edit_users');
            $role->add_cap('list_users');
            $role->add_cap('promote_users');
            $role->add_cap('create_users');
            $role->add_cap('add_users');
            $role->add_cap('delete_users');
            $role->add_cap('manage_options');
        }
        add_action('admin_init','add_grav_forms');
    // ADDITIONAL GALLERY PREVIEW SIZE
        if ( function_exists( 'add_image_size' ) ) {
            add_image_size( 'gallery-thumbnail', 500, 500, true ); 
        }
        add_filter('image_size_names_choose', 'my_image_sizes');
        function my_image_sizes($sizes) {
            $addsizes = array(
            "gallery-thumbnail" => __( "Gallery Thumbnail")
        );
        $newsizes = array_merge($sizes, $addsizes);
        return $newsizes;
        }
//SETUP WORDPRESS & BACKEND


//CUSTOMIZE CONTENT FOR CLIENT
    // ENABLE SHORTCODES IN WIDGETS
        add_filter('widget_text', 'do_shortcode');
    // PRIMARY BUTTON SHORTCODE
        function primary_button($atts, $content = null) {
            extract( shortcode_atts( array(
                'target' => '',
                'url' => '#',
                'arrow' => ''
            ), $atts ) );
            return '<a target="'.$target.'" href="'.$url.'" class="primary-button '.$arrow.'">' . do_shortcode($content) . '</a>';
        }
        add_shortcode('primary-btn', 'primary_button');
    // CARRY OVER: ACTIONBUTTON 
        function action_button($atts, $content = null) {
            extract( shortcode_atts( array(
                'target' => '',
                'link' => '#',
                'arrow' => ''
            ), $atts ) );
            return '<a target="'.$target.'" href="'.$link.'" class="primary-button '.$arrow.'">' . do_shortcode($content) . '</a>';
        }
        add_shortcode('actionbutton', 'action_button');
    // SECONDARY BUTTON SHORTCODE
        function secondary_button($atts, $content = null) {
            extract( shortcode_atts( array(
                'target' => '',
                'url' => '#',
                'arrow' => ''
            ), $atts ) );
            return '<a target="'.$target.'" href="'.$url.'" class="secondary-button '.$arrow.'">' . do_shortcode($content) . '</a>';
        }
        add_shortcode('secondary-btn', 'secondary_button');
    // LEAGAL TEXT SHORTCODE
        function legal_text($atts, $content = null) {
            extract( shortcode_atts( array( ), $atts ) );
            return '<p id="legal-text">' . do_shortcode($content) . '</p>';
        }
        add_shortcode('legal-text', 'legal_text');
    // CHANGE EXCERPT LENGTH
        function wpdocs_custom_excerpt_length( $length ) {
            return 50;
        }
        add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );
    // ADD QUOTE TAXONOMY
        function create_quote_tax() {
            register_taxonomy(
                'type',
                'quotes',
                array(
                    'label' => __( 'Type' ),
                    'rewrite' => array( 'slug' => 'type' ),
                    'hierarchical' => true,
                )
            );
        }
        add_action( 'init', 'create_quote_tax' );
    // ADD WIDGET AREAS FOR CONTENT
        if ( ! function_exists( 'custom_sidebar' ) ) {
            function custom_sidebar() {
                //SLIDER WIDGET
                $args = array(
                    'id'            => 'sidebar',
                    'name'          => __( 'sidebar'),
                    'description'   => __( 'This is the sidebar found on your blog & single post pages'),
                    'class'         => 'sidebar',
                    'before_title'  => '<h3 class="sidebar-title">',
                    'after_title'   => '</h3>',
                    'before_widget' => '<div id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</div>',
                );
                register_sidebar( $args );
            }
        add_action( 'widgets_init', 'custom_sidebar' );
        }
    // CHECK IF USER HAS ANY POSTS
        function user_has_posts($user_id) {
          $result = new WP_Query(array(
            'author'=>$user_id,
            'post_type'=>'post',
            'post_status'=>'publish',
            'category_name' => 'success-stories',
            'posts_per_page'=>1,
          ));
          return (count($result->posts)!=0);
        }
//CUSTOMIZE CONTENT FOR CLIENT
function nearest_location_number()
{
    $user_location = $_SERVER['REMOTE_ADDR'];
    $location_data = json_decode(file_get_contents('http://freegeoip.net/json/'.$user_location));
    if(!empty($location_data)) {
        global $wpdb;
        $distance = get_option('wpcl_distance');
        if ($distance != '') {
            $location_results = $wpdb->get_results( 'SELECT id, ( 3959 * acos( cos( radians('.$location_data->latitude.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$location_data->longitude.') ) + sin( radians('.$location_data->latitude.') ) * sin( radians( latitude ) ) ) ) AS distance FROM wp_contact_locations HAVING distance < '.$distance.' ORDER BY distance LIMIT 0 , 20;', OBJECT );
        } else {
            $location_results = $wpdb->get_results( 'SELECT id, ( 3959 * acos( cos( radians('.$location_data->latitude.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$location_data->longitude.') ) + sin( radians('.$location_data->latitude.') ) * sin( radians( latitude ) ) ) ) AS distance FROM wp_contact_locations ORDER BY distance LIMIT 0 , 20;', OBJECT );
        }
        if(!empty($location_results)) {
            $near_location = $wpdb->get_results( 'SELECT contact FROM wp_contact_locations WHERE id = '.$location_results[0]->id, OBJECT );
            $near_contact = $near_location[0]->contact;
            if (!empty($near_contact)) {
                return $near_contact;
            }
        } else {
            return '+1 (877) 423-7348';
        }
    }
}
add_action('admin_init', 'nearest_distance');  
function nearest_distance() {  
    add_settings_section(  
        'nearest_distance_section',
        'Distance from Locations',
        'wpcl_options_callback',
        'general'
    );

    add_settings_field(
        'wpcl_distance',
        'Distance in miles',
        'wpcl_callback',
        'general',
        'nearest_distance_section',
        array(
            'wpcl_distance'
        )  
    ); 

    register_setting('general','wpcl_distance', 'esc_attr');
}

function wpcl_options_callback() {
}

function wpcl_callback($args) {
    $option = get_option($args[0]);
    echo '<input type="text" id="'. $args[0] .'" name="'. $args[0] .'" value="' . $option . '" />';
}
?>