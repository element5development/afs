<?php /*
SUCCESS STORIES PREVIEW FORMATTING USING AJAX AND REFRESH TEMPLATE
*/ ?>

<section class="sucess-stories-preview full-width">
	<div class="max-width">

		<h2>Real people. Real stories. <strong>Real Results.</strong></h2>

		<div id="stories-refresh" class="sucess-stories-preview-container">

			<?php $posts = get_field('success_story'); ?>

			<?php if( $posts ): ?>
			<?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
			<?php setup_postdata($post); ?>
			<a href="<?php the_permalink(); ?>">
				<article class="sucess-story-preview one-third">
					<div class="name-pic">
						<img src="<?php the_field( 'headshot' ) ?>" />
						<h4><?php the_field( 'name' ) ?></h4>
					</div>
					<div class="quote">
						<p><?php the_field( 'testimony' ) ?></p>
						<a class="full-story" href="<?php the_permalink(); ?>">read full story</a>
					</div>
					<div style="clear: both"></div>
				</article>
			</a>
			<?php endforeach; ?>
			<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
			<?php endif; ?>
		</div>
		<a href="/category/success-stories/" class="secondary-button arrow">Show Me More</a>

	</div>
</section>