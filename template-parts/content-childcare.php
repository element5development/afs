<?php /*
FULLWIDTH BLUE BAR WITH INFORMATION ON AFS IN-HOUSE CHILDCARE
*/ ?>

<section class="childcare">
  <div class="childcare-contents"><?php the_field('childcare', 'options'); ?></div>
</section>