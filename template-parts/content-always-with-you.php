<?php /*
FULLWIDTH BLUE BAR WITH INFORMATION ON AFS PHONE SERVICES
*/ ?>

<section class="always-with-you full-width">
  <div class="paralax-bg"></div>
  <h2><?php the_field('with_you_title', 'option'); ?></h2>  
  <p><?php the_field('with_you_description', 'option'); ?></p>

</section>
