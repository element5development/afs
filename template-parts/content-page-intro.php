<?php /*
INTRO TEXT
*/ ?>

<section class="page-intro full-width">
	<div class="max-width">

    <?php the_field('introduction_text'); ?>

	</div>
</section>
