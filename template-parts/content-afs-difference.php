<?php /*
COMPARING AFS AND A BIG BOX GYM USING ACF
*/ ?>

<section class="why-different history full-width">

	<div class="section-header full-width">
		<h2><?php the_field('main_title'); ?></h2>
	</div>

	<div class="history-contents">
		<div class="one-half history-image" style="background-image: url(<?php the_field('stacked_image'); ?>)"></div>
		<div class="one-half history-text"><?php the_field('stacked_text'); ?></div>
		<div style="clear: both"></div>
	</div>

</section>