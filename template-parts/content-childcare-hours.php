<?php /*
CHILDCARE HOURS FOR EVERY LOCATION
*/ ?>

<section id="childcare-hours-container" class="childcare-hours-container">
	<div class="max-width">

		<h2>Hours</h2>
		<div class="childcare-hours one-third">
			<h3>Ann Arbor <b>Hours</b></h3>
			<ul>
	      <li><span>M</span>  <?php the_field('ann_arbor_monday'); ?></li>
	      <li><span>TU</span>  <?php the_field('ann_arbor_tuesday'); ?></li>
	      <li><span>W</span> <?php the_field('ann_arbor_wednesday'); ?></li>
	      <li><span>TH</span>  <?php the_field('ann_arbor_thursday'); ?></li>
	      <li><span>F</span> <?php the_field('ann_arbor_friday'); ?></li>
	      <li><span>SA</span>  <?php the_field('ann_arbor_saturday'); ?></li>
	      <li><span>SU</span>  <?php the_field('ann_arbor_sunday'); ?></li>
	    </ul>
		</div>
		<div class="childcare-hours one-third">
			<h3>Plymouth <b>Hours</b></h3>
			<ul>
	      <li><span>M</span> <?php the_field('plymouth_monday'); ?></li>
	      <li><span>TU</span>  <?php the_field('plymouth_tuesday'); ?></li>
	      <li><span>W</span> <?php the_field('plymouth_wednesday'); ?></li>
	      <li><span>TH</span>  <?php the_field('plymouth_thursday'); ?></li>
	      <li><span>F</span> <?php the_field('plymouth_friday'); ?></li>
	      <li><span>SA</span>  <?php the_field('plymouth_saturday'); ?></li>
	      <li><span>SU</span>  <?php the_field('plymouth_sunday'); ?></li>
	    </ul>
		</div>
		<div class="childcare-hours one-third">
			<h3>Rochester <b>Hours</b></h3>
			<ul>
	      <li><span>M</span> <?php the_field('rochester_monday'); ?></li>
	      <li><span>TU</span>  <?php the_field('rochester_tuesday'); ?></li>
	      <li><span>W</span> <?php the_field('rochester_wednesday'); ?></li>
	      <li><span>TH</span>  <?php the_field('rochester_thursday'); ?></li>
	      <li><span>F</span> <?php the_field('rochester_friday'); ?></li>
	      <li><span>SA</span>  <?php the_field('rochester_saturday'); ?></li>
	      <li><span>SU</span>  <?php the_field('rochester_sunday'); ?></li>
	    </ul>
		</div>
		<div style="clear: both"></div>

	</div>
</section>
