<?php /*
BOX LIST OF ITEMS INCLUDED WITH A MEMBERSHIP
*/ ?>

<section class="membership-include full-width">

	<div class="section-header full-width">
		<h2><strong>Become a member</strong> of our community</h2>
	</div>

	<div class="include-container full-width">
		<div class="max-width">

			<h4>All members have access to:</h4>
			
			<?php if( have_rows('include') ) {
	      while ( have_rows('include') ) : the_row(); ?>

	        <div class="include-item one-third">
	        	<div class="vertical-center-parent">
	        		<?php $image = get_sub_field('include_icon'); ?>
	        		<div class="icon-container"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></div>
	        		<div class="vertical-center"><p><?php the_sub_field('include_content'); ?></p></div>
	        	</div>
	        </div>

	      <?php endwhile;
	    } else { ?>
	      <!--no content -->>
	    <?php } ?>

    	<div style="clear: both"></div>
		</div>
	</div>

</section>