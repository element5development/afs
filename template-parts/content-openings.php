<?php /*
SORTABLE TABLE OF JOB OPENINGS
*/ ?>
<section class="openings full-width">
  <div class="max-width">

    <h2>Current <b>Openings</b></h2>


    <table id="myTable" class="tablesorter">
      <thead>
        <tr>
          <th>Position</th>
          <th>Compensation</th>
          <th>Location</th>
          <th>Apply</th>
        </tr>
      </thead>
      <tbody>

      <?php if( have_rows('openings') ) {
        while ( have_rows('openings') ) : the_row(); ?>

          <tr>
            <td><?php the_sub_field('position'); ?></td>
            <td><?php the_sub_field('salary'); ?></td>
            <td><?php the_sub_field('location'); ?></td>
            <td>
              <?php if ( get_sub_field('filled') ) { ?>
                <b>position filled</b>
              <?php } else { ?>
                <a class="primary-button arrow" href="/afs-careers/apply-today/?position=<?php the_sub_field('position'); ?>&location=<?php the_sub_field('location'); ?>">APPLY FOR THIS</a>
              <?php } ?>
            </td>
          </tr>

        <?php endwhile;
      } else { ?>
        <!--no content -->>
      <?php } ?>

      </tbody>
    </table>

    <div class="mobile-openings">

      <?php if( have_rows('openings') ) {
        while ( have_rows('openings') ) : the_row(); ?>

          <div class="position">
            <p><span>Position: </span><?php the_sub_field('position'); ?></p>
            <p><span>Salary: </span><?php the_sub_field('salary'); ?></p>
            <p><span>Location: </span><?php the_sub_field('location'); ?></p>
            <p>
                <?php if ( get_sub_field('filled') ) { ?>
                  <span>position filled</span>
                <?php } else { ?>
                  <a class="primary-button arrow" href="/afs-careers/apply-today/?position=<?php the_sub_field('position'); ?>&location=<?php the_sub_field('location'); ?>">APPLY FOR THIS</a>
                <?php } ?>
            </p>
          </div>

        <?php endwhile;
      } else { ?>
        <!--no content -->>
      <?php } ?>
      
    </div>



  </div>
</section>