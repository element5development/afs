<?php /*
DIGITAL CONSULTATION OR IN-PERSON CONSULATION CALL-TO-ACTIONS
*/ ?>

<section class="consultation simplified full-width">

	<div class="consultation-container consultation-container-simplied">
		<div class="one-half">
			<a href="/digital-consultation/" class="secondary-button arrow">give me a digital consultation</a>
		</div>
		<div class="or-split">OR</div>
		<div class="one-half">
			<a href="/in-person-consultation/" class="secondary-button arrow">get your free consultation</a>
		</div>
		<div style="clear: both"></div>
	</div>

</section>