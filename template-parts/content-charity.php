<?php /*
FULLWIDTH BLUE BAR WITH INFORMATION ON AFS IN-HOUSE CHILDCARE
*/ ?>

<section class="charity-challenge-cta">

  <?php if( have_rows('location_charities') ) { ?>
    <div class="location-charities max-width">
      <?php 
      $i = 0;
      while ( have_rows('location_charities') ) : $i++; the_row(); ?>

        <a target="_blank" href="<?php the_sub_field('charity_url') ?>">
          <div class="charity one-third">
            <?php if ( $i == 1 ) { ?>
              <h3>Rochester</h3>
            <?php } elseif ( $i == 2 ) { ?>
              <h3>Ann Arbor</h3>
            <?php } else { ?>
              <h3>Plymouth</h3>
            <?php } ?>
            <h2><?php the_sub_field('charity_name'); ?></h2>
            <?php $image = get_sub_field('charity_logo'); ?>
            <div class="charity-logo"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></div>
            <?php if ( get_sub_field('charity_video') ) { ?>
              <a class="fancybox fancybox.iframe primary-button" href="<?php the_sub_field('charity_video'); ?>" data-fancybox-type="iframe">Learn More</a>
            <?php } ?>
          </div>
        </a>

      <?php endwhile; ?>
      <div style="clear: both"></div>
    </div>
  <?php } else { ?>
    <!--no content -->>
  <?php } ?>


  <div class="charity-challenge-bg"></div>
  <div class="max-width"><?php the_field('charity_challenge_cta'); ?></div>
</section>