<?php /*
LOCATION TILES
*/ ?>

<section class="locations full-width">
  <div class="locations-container max-width hidden">

      <?php if( have_rows('locations') ) {
        while ( have_rows('locations') ) : $i++; the_row(); ?>

            <a href="<?php the_sub_field('location_google_map') ?>">
              <div class="location one-third">
                <h2><?php the_sub_field('location_title'); ?></h2>
                  <?php $image = get_sub_field('location_image'); ?>
                  <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                <h4><?php the_sub_field('location_address'); ?></h4>
                <?php the_sub_field('location_services'); ?>
              </div>
            </a>

        <?php endwhile;
      } else { ?>
        <!--no content -->>
      <?php } ?>
      <div style="clear: both"></div>

  </div>
</section>