<?php /*
DIGITAL CONSULTATION FORM
*/ ?>

<section class="digital-consultaion-container max-width">

	<div class="rating-system">
		<img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-face-happy.png" alt="happy face"/>
		<div class="spacer"></div>
		<img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-face-medium.png" alt="blah face" />
		<div class="spacer"></div>
		<img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-face-sad.png" alt="sad face"/>
	</div>
	<div id="digital-consultaion-form" class="digital-consultaion-form">
		<div class="first-options">
			<div id="first-option-1" class="first-option">I need help rehabbing and injury</div>
			<div id="first-option-2" class="first-option">I'm in good shape, but need to go the next level.</div>
			<div id="first-option-3" class="first-option">I don't know where to start</div>
			<div id="first-option-4" class="first-option">I want to keep up with my kids/grandkids</div>
			<div id="first-option-5" class="first-option">I need help keeping weight off as I get older</div>
			<div id="first-option-6" class="first-option">I want my clothes to fit like they use to!</div>
			<div id="first-option-7" class="first-option">I need someone to help me stay motivated</div>
			<div id="first-option-8" class="first-option">I want to look better on vacation</div>
			<div id="first-option-9" class="first-option">I am training for a specific sport or event</div>
			<div id="first-option-10" class="first-option">I want to live a long, healthy life</div>
			<div id="first-option-11" class="first-option">I need a safe resistance training program</div>
			<div id="first-option-12" class="first-option">I want my confidence back</div>
			<div id="first-option-13" class="first-option">I am an emotional eater</div>
			<div id="first-option-14" class="first-option">My diet needs work!</div>
		</div>
		<div class="second-options">
			<div id="second-option-1" class="second-option">I need help rehabbing and injury</div>
			<div id="second-option-2" class="second-option">I'm in good shape, but need to go the next level.</div>
			<div id="second-option-3" class="second-option">I don't know where to start</div>
			<div id="second-option-4" class="second-option">I want to keep up with my kids/grandkids</div>
			<div id="second-option-5" class="second-option">I need help keeping weight off as I get older</div>
			<div id="second-option-6" class="second-option">I want my clothes to fit like they use to!</div>
			<div id="second-option-7" class="second-option">I need someone to help me stay motivated</div>
			<div id="second-option-8" class="second-option">I want to look better on vacation</div>
			<div id="second-option-9" class="second-option">I am training for a specific sport or event</div>
			<div id="second-option-10" class="second-option">I want to live a long, healthy life</div>
			<div id="second-option-11" class="second-option">I need a safe resistance training program</div>
			<div id="second-option-12" class="second-option">I want my confidence back</div>
			<div id="second-option-13" class="second-option">I am an emotional eater</div>
			<div id="second-option-14" class="second-option">My diet needs work!</div>
		</div>
		<div class="third-options">
			<div id="third-option-1" class="third-option">I need help rehabbing and injury</div>
			<div id="third-option-2" class="third-option">I'm in good shape, but need to go the next level.</div>
			<div id="third-option-3" class="third-option">I don't know where to start</div>
			<div id="third-option-4" class="third-option">I want to keep up with my kids/grandkids</div>
			<div id="third-option-5" class="third-option">I need help keeping weight off as I get older</div>
			<div id="third-option-6" class="third-option">I want my clothes to fit like they use to!</div>
			<div id="third-option-7" class="third-option">I need someone to help me stay motivated</div>
			<div id="third-option-8" class="third-option">I want to look better on vacation</div>
			<div id="third-option-9" class="third-option">I am training for a specific sport or event</div>
			<div id="third-option-10" class="third-option">I want to live a long, healthy life</div>
			<div id="third-option-11" class="third-option">I need a safe resistance training program</div>
			<div id="third-option-12" class="third-option">I want my confidence back</div>
			<div id="third-option-13" class="third-option">I am an emotional eater</div>
			<div id="third-option-14" class="third-option">My diet needs work!</div>
		</div>
		<div class="fourth-options">
			<div id="fourth-option-1" class="fourth-option">I need help rehabbing and injury</div>
			<div id="fourth-option-2" class="fourth-option">I'm in good shape, but need to go the next level.</div>
			<div id="fourth-option-3" class="fourth-option">I don't know where to start</div>
			<div id="fourth-option-4" class="fourth-option">I want to keep up with my kids/grandkids</div>
			<div id="fourth-option-5" class="fourth-option">I need help keeping weight off as I get older</div>
			<div id="fourth-option-6" class="fourth-option">I want my clothes to fit like they use to!</div>
			<div id="fourth-option-7" class="fourth-option">I need someone to help me stay motivated</div>
			<div id="fourth-option-8" class="fourth-option">I want to look better on vacation</div>
			<div id="fourth-option-9" class="fourth-option">I am training for a specific sport or event</div>
			<div id="fourth-option-10" class="fourth-option">I want to live a long, healthy life</div>
			<div id="fourth-option-11" class="fourth-option">I need a safe resistance training program</div>
			<div id="fourth-option-12" class="fourth-option">I want my confidence back</div>
			<div id="fourth-option-13" class="fourth-option">I am an emotional eater</div>
			<div id="fourth-option-14" class="fourth-option">My diet needs work!</div>
		</div>
		<div class="five-options">
			<div id="five-option-1" class="five-option">I need help rehabbing and injury</div>
			<div id="five-option-2" class="five-option">I'm in good shape, but need to go the next level.</div>
			<div id="five-option-3" class="five-option">I don't know where to start</div>
			<div id="five-option-4" class="five-option">I want to keep up with my kids/grandkids</div>
			<div id="five-option-5" class="five-option">I need help keeping weight off as I get older</div>
			<div id="five-option-6" class="five-option">I want my clothes to fit like they use to!</div>
			<div id="five-option-7" class="five-option">I need someone to help me stay motivated</div>
			<div id="five-option-8" class="five-option">I want to look better on vacation</div>
			<div id="five-option-9" class="five-option">I am training for a specific sport or event</div>
			<div id="five-option-10" class="five-option">I want to live a long, healthy life</div>
			<div id="five-option-11" class="five-option">I need a safe resistance training program</div>
			<div id="five-option-12" class="five-option">I want my confidence back</div>
			<div id="five-option-13" class="five-option">I am an emotional eater</div>
			<div id="five-option-14" class="five-option">My diet needs work!</div>
		</div>
		<div class="six-options">
			<div id="six-option-1" class="six-option">I need help rehabbing and injury</div>
			<div id="six-option-2" class="six-option">I'm in good shape, but need to go the next level.</div>
			<div id="six-option-3" class="six-option">I don't know where to start</div>
			<div id="six-option-4" class="six-option">I want to keep up with my kids/grandkids</div>
			<div id="six-option-5" class="six-option">I need help keeping weight off as I get older</div>
			<div id="six-option-6" class="six-option">I want my clothes to fit like they use to!</div>
			<div id="six-option-7" class="six-option">I need someone to help me stay motivated</div>
			<div id="six-option-8" class="six-option">I want to look better on vacation</div>
			<div id="six-option-9" class="six-option">I am training for a specific sport or event</div>
			<div id="six-option-10" class="six-option">I want to live a long, healthy life</div>
			<div id="six-option-11" class="six-option">I need a safe resistance training program</div>
			<div id="six-option-12" class="six-option">I want my confidence back</div>
			<div id="six-option-13" class="six-option">I am an emotional eater</div>
			<div id="six-option-14" class="six-option">My diet needs work!</div>
		</div>
		<div class="seven-options">
			<div id="seven-option-1" class="seven-option">I need help rehabbing and injury</div>
			<div id="seven-option-2" class="seven-option">I'm in good shape, but need to go the next level.</div>
			<div id="seven-option-3" class="seven-option">I don't know where to start</div>
			<div id="seven-option-4" class="seven-option">I want to keep up with my kids/grandkids</div>
			<div id="seven-option-5" class="seven-option">I need help keeping weight off as I get older</div>
			<div id="seven-option-6" class="seven-option">I want my clothes to fit like they use to!</div>
			<div id="seven-option-7" class="seven-option">I need someone to help me stay motivated</div>
			<div id="seven-option-8" class="seven-option">I want to look better on vacation</div>
			<div id="seven-option-9" class="seven-option">I am training for a specific sport or event</div>
			<div id="seven-option-10" class="seven-option">I want to live a long, healthy life</div>
			<div id="seven-option-11" class="seven-option">I need a safe resistance training program</div>
			<div id="seven-option-12" class="seven-option">I want my confidence back</div>
			<div id="seven-option-13" class="seven-option">I am an emotional eater</div>
			<div id="seven-option-14" class="seven-option">My diet needs work!</div>
		</div>
		<div class="eight-options">
			<div id="eight-option-1" class="eight-option">I need help rehabbing and injury</div>
			<div id="eight-option-2" class="eight-option">I'm in good shape, but need to go the next level.</div>
			<div id="eight-option-3" class="eight-option">I don't know where to start</div>
			<div id="eight-option-4" class="eight-option">I want to keep up with my kids/grandkids</div>
			<div id="eight-option-5" class="eight-option">I need help keeping weight off as I get older</div>
			<div id="eight-option-6" class="eight-option">I want my clothes to fit like they use to!</div>
			<div id="eight-option-7" class="eight-option">I need someone to help me stay motivated</div>
			<div id="eight-option-8" class="eight-option">I want to look better on vacation</div>
			<div id="eight-option-9" class="eight-option">I am training for a specific sport or event</div>
			<div id="eight-option-10" class="eight-option">I want to live a long, healthy life</div>
			<div id="eight-option-11" class="eight-option">I need a safe resistance training program</div>
			<div id="eight-option-12" class="eight-option">I want my confidence back</div>
			<div id="eight-option-13" class="eight-option">I am an emotional eater</div>
			<div id="eight-option-14" class="eight-option">My diet needs work!</div>
		</div>
		<div class="nine-options">
			<div id="nine-option-1" class="nine-option">I need help rehabbing and injury</div>
			<div id="nine-option-2" class="nine-option">I'm in good shape, but need to go the next level.</div>
			<div id="nine-option-3" class="nine-option">I don't know where to start</div>
			<div id="nine-option-4" class="nine-option">I want to keep up with my kids/grandkids</div>
			<div id="nine-option-5" class="nine-option">I need help keeping weight off as I get older</div>
			<div id="nine-option-6" class="nine-option">I want my clothes to fit like they use to!</div>
			<div id="nine-option-7" class="nine-option">I need someone to help me stay motivated</div>
			<div id="nine-option-8" class="nine-option">I want to look better on vacation</div>
			<div id="nine-option-9" class="nine-option">I am training for a specific sport or event</div>
			<div id="nine-option-10" class="nine-option">I want to live a long, healthy life</div>
			<div id="nine-option-11" class="nine-option">I need a safe resistance training program</div>
			<div id="nine-option-12" class="nine-option">I want my confidence back</div>
			<div id="nine-option-13" class="nine-option">I am an emotional eater</div>
			<div id="nine-option-14" class="nine-option">My diet needs work!</div>
		</div>
		<div class="ten-options">
			<div id="ten-option-1" class="ten-option">I need help rehabbing and injury</div>
			<div id="ten-option-2" class="ten-option">I'm in good shape, but need to go the next level.</div>
			<div id="ten-option-3" class="ten-option">I don't know where to start</div>
			<div id="ten-option-4" class="ten-option">I want to keep up with my kids/grandkids</div>
			<div id="ten-option-5" class="ten-option">I need help keeping weight off as I get older</div>
			<div id="ten-option-6" class="ten-option">I want my clothes to fit like they use to!</div>
			<div id="ten-option-7" class="ten-option">I need someone to help me stay motivated</div>
			<div id="ten-option-8" class="ten-option">I want to look better on vacation</div>
			<div id="ten-option-9" class="ten-option">I am training for a specific sport or event</div>
			<div id="ten-option-10" class="ten-option">I want to live a long, healthy life</div>
			<div id="ten-option-11" class="ten-option">I need a safe resistance training program</div>
			<div id="ten-option-12" class="ten-option">I want my confidence back</div>
			<div id="ten-option-13" class="ten-option">I am an emotional eater</div>
			<div id="ten-option-14" class="ten-option">My diet needs work!</div>
		</div>
		<div class="eleven-options">
			<div id="eleven-option-1" class="eleven-option">I need help rehabbing and injury</div>
			<div id="eleven-option-2" class="eleven-option">I'm in good shape, but need to go the next level.</div>
			<div id="eleven-option-3" class="eleven-option">I don't know where to start</div>
			<div id="eleven-option-4" class="eleven-option">I want to keep up with my kids/grandkids</div>
			<div id="eleven-option-5" class="eleven-option">I need help keeping weight off as I get older</div>
			<div id="eleven-option-6" class="eleven-option">I want my clothes to fit like they use to!</div>
			<div id="eleven-option-7" class="eleven-option">I need someone to help me stay motivated</div>
			<div id="eleven-option-8" class="eleven-option">I want to look better on vacation</div>
			<div id="eleven-option-9" class="eleven-option">I am training for a specific sport or event</div>
			<div id="eleven-option-10" class="eleven-option">I want to live a long, healthy life</div>
			<div id="eleven-option-11" class="eleven-option">I need a safe resistance training program</div>
			<div id="eleven-option-12" class="eleven-option">I want my confidence back</div>
			<div id="eleven-option-13" class="eleven-option">I am an emotional eater</div>
			<div id="eleven-option-14" class="eleven-option">My diet needs work!</div>
		</div>
		<div class="twelve-options">
			<div id="twelve-option-1" class="twelve-option">I need help rehabbing and injury</div>
			<div id="twelve-option-2" class="twelve-option">I'm in good shape, but need to go the next level.</div>
			<div id="twelve-option-3" class="twelve-option">I don't know where to start</div>
			<div id="twelve-option-4" class="twelve-option">I want to keep up with my kids/grandkids</div>
			<div id="twelve-option-5" class="twelve-option">I need help keeping weight off as I get older</div>
			<div id="twelve-option-6" class="twelve-option">I want my clothes to fit like they use to!</div>
			<div id="twelve-option-7" class="twelve-option">I need someone to help me stay motivated</div>
			<div id="twelve-option-8" class="twelve-option">I want to look better on vacation</div>
			<div id="twelve-option-9" class="twelve-option">I am training for a specific sport or event</div>
			<div id="twelve-option-10" class="twelve-option">I want to live a long, healthy life</div>
			<div id="twelve-option-11" class="twelve-option">I need a safe resistance training program</div>
			<div id="twelve-option-12" class="twelve-option">I want my confidence back</div>
			<div id="twelve-option-13" class="twelve-option">I am an emotional eater</div>
			<div id="twelve-option-14" class="twelve-option">My diet needs work!</div>
		</div>
		<div class="thirteen-options">
			<div id="thirteen-option-1" class="thirteen-option">I need help rehabbing and injury</div>
			<div id="thirteen-option-2" class="thirteen-option">I'm in good shape, but need to go the next level.</div>
			<div id="thirteen-option-3" class="thirteen-option">I don't know where to start</div>
			<div id="thirteen-option-4" class="thirteen-option">I want to keep up with my kids/grandkids</div>
			<div id="thirteen-option-5" class="thirteen-option">I need help keeping weight off as I get older</div>
			<div id="thirteen-option-6" class="thirteen-option">I want my clothes to fit like they use to!</div>
			<div id="thirteen-option-7" class="thirteen-option">I need someone to help me stay motivated</div>
			<div id="thirteen-option-8" class="thirteen-option">I want to look better on vacation</div>
			<div id="thirteen-option-9" class="thirteen-option">I am training for a specific sport or event</div>
			<div id="thirteen-option-10" class="thirteen-option">I want to live a long, healthy life</div>
			<div id="thirteen-option-11" class="thirteen-option">I need a safe resistance training program</div>
			<div id="thirteen-option-12" class="thirteen-option">I want my confidence back</div>
			<div id="thirteen-option-13" class="thirteen-option">I am an emotional eater</div>
			<div id="thirteen-option-14" class="thirteen-option">My diet needs work!</div>
		</div>
		<div class="fourteen-options">
			<div id="fourteen-option-1" class="fourteen-option">I need help rehabbing and injury</div>
			<div id="fourteen-option-2" class="fourteen-option">I'm in good shape, but need to go the next level.</div>
			<div id="fourteen-option-3" class="fourteen-option">I don't know where to start</div>
			<div id="fourteen-option-4" class="fourteen-option">I want to keep up with my kids/grandkids</div>
			<div id="fourteen-option-5" class="fourteen-option">I need help keeping weight off as I get older</div>
			<div id="fourteen-option-6" class="fourteen-option">I want my clothes to fit like they use to!</div>
			<div id="fourteen-option-7" class="fourteen-option">I need someone to help me stay motivated</div>
			<div id="fourteen-option-8" class="fourteen-option">I want to look better on vacation</div>
			<div id="fourteen-option-9" class="fourteen-option">I am training for a specific sport or event</div>
			<div id="fourteen-option-10" class="fourteen-option">I want to live a long, healthy life</div>
			<div id="fourteen-option-11" class="fourteen-option">I need a safe resistance training program</div>
			<div id="fourteen-option-12" class="fourteen-option">I want my confidence back</div>
			<div id="fourteen-option-13" class="fourteen-option">I am an emotional eater</div>
			<div id="fourteen-option-14" class="fourteen-option">My diet needs work!</div>
		</div>
		<div class="blank-options">
			<div id="blank-option-1" class="blank-option">I need help rehabbing and injury</div>
			<div id="blank-option-2" class="blank-option">I'm in good shape, but need to go the next level.</div>
			<div id="blank-option-3" class="blank-option">I don't know where to start</div>
			<div id="blank-option-4" class="blank-option">I want to keep up with my kids/grandkids</div>
			<div id="blank-option-5" class="blank-option">I need help keeping weight off as I get older</div>
			<div id="blank-option-6" class="blank-option">I want my clothes to fit like they use to!</div>
			<div id="blank-option-7" class="blank-option">I need someone to help me stay motivated</div>
			<div id="blank-option-8" class="blank-option">I want to look better on vacation</div>
			<div id="blank-option-9" class="blank-option">I am training for a specific sport or event</div>
			<div id="blank-option-10" class="blank-option">I want to live a long, healthy life</div>
			<div id="blank-option-11" class="blank-option">I need a safe resistance training program</div>
			<div id="blank-option-12" class="blank-option">I want my confidence back</div>
			<div id="blank-option-13" class="blank-option">I am an emotional eater</div>
			<div id="blank-option-14" class="blank-option">My diet needs work!</div>
		</div>
	</div>
	<a href="/digital-consultation/digital-consultation-results/" id="digital-results" class="primary-button arrow selected">Score Me</a>
	<a href="/digital-consultation/digital-consultation-results/?result=wl" id="wl-results" class="primary-button arrow">Score Me</a>
	<a href="/digital-consultation/digital-consultation-results/?result=pc" id="pc-results" class="primary-button arrow">Score Me</a>
	<a href="/digital-consultation/digital-consultation-results/?result=ss" id="ss-results" class="primary-button arrow">Score Me</a>
	<a id="digital-reset" class="reset-form secondary-button">Reset</a>

</section>
