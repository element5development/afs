<?php /*
WEIGHT LOSS BLUE BAR, PRICEING AND INTRODUCTION
*/ ?>

<section class="weight-loss-bar full-width">
	<div class="max-width">
		<?php the_field('blue_bar_intro'); ?>
	</div>
</section>


<section class="weight-loss">
	<div id="program-details"></div>
	<div class="max-width">

		<?php if ( get_field('price_per_session') ) { ?>
		<div class="one-half pricing">
			<div class="price-table-option weight-loss-pricing hidden">
				<?php if ( is_page(15434) ) { ?>
				<h3><?php the_title(); ?> Options</h3>
				<h4 class="price-container"><span
						class="option-price"><?php the_field('price_per_session'); ?></span><br />Lunch & Learn</h4>
				<?php } else { ?>
				<h3><?php the_title(); ?> Classes</h3>
				<h4 class="price-container">Starting at<br /><span
						class="option-price"><?php the_field('price_per_session'); ?></span><br />per month</h4>
				<?php } ?>
				<div class="option-contents">
					<?php the_field('price_description'); ?>
				</div>
			</div>
		</div>
		<?php } ?>
		<div class="one-half">
			<?php the_field('intro_content'); ?>
		</div>
		<div style="clear: both"></div>

	</div>
</section>