<?php /*
CHILDCARE HOURS FOR EVERY LOCATION
*/ ?>

<section class="childcare-services">
  <div class="max-width">

  	<div class="one-half">
  		<div class="childcare-price hidden">
      	<h3>Childcare Services</h3>
      	<h4 class="price-container"><span class="option-price"><?php the_field('month_price'); ?></span><br/>per month</h4>
      	<div class="option-contents">
      		<hr>
      		<p>One Child: <span><?php the_field('one_child_price'); ?></span></p>
      		<hr>
      		<p>Additional Child: <span><?php the_field('additional_child_price'); ?></span></p>
      	</div>
      </div>
  	</div>
  	<div class="one-half">
  		<?php the_field('childcare_services_content'); ?>
      <a href="#childcare-hours-container" class="primary-button smoothScroll">view childcare hours</a>
  	</div>
  	<div style="clear: both"></div>

  </div>
</section>
