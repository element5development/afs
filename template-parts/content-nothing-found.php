<?php /*
NO RESULTS FOUND
*/ ?>

<section class="nothing-found">
	<h2>Nothing Found</h2>
	<p>Try starting with one of our more popular pages</p>
	<?php 
		$args = array(
		   'post_type' => 'page',
		   'post__in' => array( 11324, 38, 10993, 11220, 37, 11127 )
		);

		$the_query = new WP_Query( $args );
		while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

			<?php //USE FEATURED IAMGE OTHERWISE USE DEFAULT IAMGE
				$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' ); 
				if ( $src[0] == "") {
					$featuredimg = "/wp-content/themes/afs/img/default-blog-img.jpg";
				} else {
					$featuredimg = $src[0];
				}	
			?>
			<a href="<?php the_permalink(); ?>">
				<article id="post-<?php the_ID(); ?>" class="search-post one-third grid-item no-found-search">
					<img class="featured background-check" src="<?php echo $featuredimg; ?>" />
					<h3><?php the_title(); ?></h3>
				</article>
			</a>

	<?php endwhile; ?>
</section>
