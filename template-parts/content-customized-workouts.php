<?php /*
FULLWIDTH BLUE BAR WITH INFORMATION ON AFS CUSTOMIZED WORKOUTS
*/ ?>

<section class="customized-workouts full-width">

	<h2><?php the_field('customized_workouts', 'option'); ?></h2>

</section>
