<?php /*
LOCATION DETAILS
*/ ?>

<section class="location-details full-width">
  <div class="max-width">

  <div class="two-third">
    <?php the_field('google_map_iframe') ?>
  </div>
  <div class="one-third details">
    <h2><?php the_field('location_title'); ?></h2>
    <p><?php the_field('location_address'); ?></p>
    <a href="<?php the_field('google_map_link'); ?>" class="secondary-button arrow">get directions</a>
    <?php if ( is_page(11152) ) { ?>
       <a href="/contact-us/?location=Rochester" class="secondary-button arrow">Contact Us</a>
    <?php } else { ?>
      <a href="tel:<?php the_field('location_phone'); ?>" class="secondary-button phone"><?php the_field('location_phone'); ?></a>
    <?php } ?>
    <?php if ( !is_page(11152) ) { ?>
      <p>Features at this location:</p>
    <?php } ?>
    <?php the_field('location_features'); ?>
  </div>
  <div style="clear: both"></div>

  </div>
</section>