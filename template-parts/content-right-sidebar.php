<?php /*
RIGHT SIDEBAR CONTENT
*/ ?>

<aside id="sidebar-right" class="sidebar sidebar-right">
	<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('sidebar')) : else : ?>
		<p><strong>Widget Ready</strong></p>  
	<?php endif; ?> 
	<div class="widget consultation-widget">
		<h3>Get a free<br/><strong>In-Person<br/>Consultation</strong></h3>
		<p>We have one goal—to get you in the best shape of your life! Whether “in shape” means losing 10 lbs to 150 lbs or setting a new marathon PR, AFS has the systems and the science that will get you there.</p>
		<a href="/" class="secondary-button arrow">Get Yours</a>
	</div>
	<div class="widget newsletter-widget">
		<h3>Sign up for<br/><strong>our Updates</strong></h3>
		<p>Stay up to date with us by subscribing to our newsletter.</p>
		<?php echo do_shortcode('[constantcontactapi formid="1"]'); ?>
	</div>
</aside>
