<?php /*
BOX LIST OF ITEMS INCLUDED WITH A MEMBERSHIP
*/ ?>

<section class="choose-a-class price-table-container full-width">

	<h2>Choose your <b>Classes</b></h2>

	<div class="table-container full-width">
		<div class="max-width">
			
			<?php if( have_rows('price_table') ) {
	      while ( have_rows('price_table') ) : the_row(); ?>

	        <a href="<?php the_sub_field('price_option_link'); ?>"><div class="price-table-option one-fourth">
	        	<h3><?php the_sub_field('price_option_title'); ?></h3>
	        	<h4 class="price-container">Starting at<br/><span class="option-price"><?php the_sub_field('price_option_price'); ?></span><br/>per month</h4>
	        	<div class="option-contents">
	        		<?php the_sub_field('price_option_details'); ?>
	        	</div>
	        </div></a>

	      <?php endwhile;
	    } else { ?>
	      <!--no content -->
	    <?php } ?>

    	<div style="clear: both"></div>
		</div>
	</div>

</section>