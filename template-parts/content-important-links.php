<?php /*
LINKS TO CUSTOM BUILT MEMBERSHIP AREA
*/ ?>

<section class="important-links">

	<h2>Important Links</h2>
	<div class="nav-container">
		<a href="https://cms.4afsfit.com/user/login" class="secondary-button arrow">Login</a>
		<a href="https://cms.4afsfit.com/password/forgot" class="secondary-button arrow">Reset Password</a>
		<a href="mailto:cmshelp@4afsfit.com" class="secondary-button arrow">Tech Support</a>
	</div>

</section>
