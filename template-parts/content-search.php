<?php /*
SEARCH PAGE RESULT FORMATTING
*/ ?>

<?php 
	//USE FEATURED IAMGE OTHERWISE USE DEFAULT IAMGE
	$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' ); 
	if ( $src[0] == "") {
		$featuredimg = "/wp-content/themes/afs/img/default-blog-img.jpg";
	} else {
		$featuredimg = $src[0];
	}	
?>

<?php 
	$post_type = get_post_type_object( get_post_type($post) );
?>

<?php if ( $post_type->labels->singular_name == 'Post'  ) { ?>

	<a href="<?php the_permalink(); ?>">
		<article class="blog-post-preview one-third grid-item">
			<img class="featured background-check" src="<?php echo $featuredimg; ?>" />
			<div class="post-info">
				<div class="post-feed-author-pic"><?php echo get_avatar( get_the_author_meta( 'ID' ), 75 ); ?></div>
				<div class="post-feed-author color-check">Written by <?php the_author(); ?> </div>
				<div class="post-date"><?php the_time('M j, Y') ?></div>
				<div style="clear: both"></div>
			</div>
			<h3><?php the_title(); ?></h3>
			<div class="post-feed-excerpt"><?php the_excerpt(); ?></div>
		</article>
	</a>

<?php } else { ?>

	<a href="<?php the_permalink(); ?>">
		<article id="post-<?php the_ID(); ?>" class="search-post one-third grid-item">
			<img class="featured background-check" src="<?php echo $featuredimg; ?>" />
			<h3><?php the_title(); ?></h3>
			<div class="post-feed-excerpt"><?php the_excerpt(); ?></div> 
		</article>
	</a>

<?php	} ?>

