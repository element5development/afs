<?php /*
DIGITAL CONSULTATION OR IN-PERSON CONSULATION CALL-TO-ACTIONS
*/ ?>
<?php if ( is_page(11168) ) { ?>

	<section class="consultation full-width">

		<div class="consultation-container">
			<div class="digital-consultation">
				<h2>Want a <b>Digital Consultation</b> instead?</h2>
				<p><?php the_field('digital_content', 'options'); ?></p>
				<a href="/digital-consultation/" class="secondary-button arrow">give me a digital consultation</a>
			</div>
		</div>

	</section>

<?php } elseif ( is_page(11220) || is_page(11253) ) { ?>

	<section class="consultation in-person full-width">

		<div class="consultation-container">
			<div class="person-consultation">
				<h2>Want an <b>In-Person Consultation</b> instead?</h2>
				<p><?php the_field('person_content', 'options'); ?></p>
				<a href="/in-person-consultation/" class="secondary-button arrow">GET YOUR FREE CONSULTATION</a>
			</div>
		</div>

	</section>

<?php } else { ?>

	<section class="consultation full-width">

		<h2>Not sure <b>where to start?</b></h2>
		<div class="consultation-container">
			<div class="digital-consultation one-half">
				<h2>Get a free <b>Digital Consultation</b></h2>
				<p><?php the_field('digital_content', 'options'); ?></p>
				<a href="/digital-consultation/" class="secondary-button arrow">give me a digital consultation</a>
			</div>
			<div class="or-split">OR</div>
			<div class="person-consultation one-half">
				<h2>Get a free <b>In-Person Consultation</b></h2>
				<p><?php the_field('person_content', 'options'); ?></p>
				<a href="/in-person-consultation/" class="secondary-button arrow">get your free consultation</a>
			</div>
			<div style="clear: both"></div>
		</div>

	</section>

<?php	} ?>