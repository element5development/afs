<?php /*
GETTING STARTED STEPS USING ACF
*/ ?>

<section class="getting-started max-width">

	<h2><b>Getting Started</b></h2>	

  <div class="steps-container">
    <?php if( have_rows('steps') ) {
      while ( have_rows('steps') ) : the_row(); ?>

        <div class="step-container">
          <div class="step-count">
            <div class="bubble">
              step<br/>
              <span><?php the_sub_field('step_number'); ?></span>
            </div>
          </div>
          <div class="orange-dot"></div>
          <div class="step-contents">
            <h4><?php the_sub_field('step_title'); ?></h4>
            <p><?php the_sub_field('step_description'); ?></p>
          </div>
          <div style="clear: both"></div>
        </div>

      <?php endwhile;
    } else { ?>
      <!--no content -->>
    <?php } ?>

    <div class="step-container final-step">
      <div class="step-count">
        <div class="bubble">
          Your<br/>
          Future
        </div>
      </div>
    </div>

    <div style="clear: both"></div>
  </div>
</section>

<section class="endless-support full-width">
  <div class="paralax-bg"></div>
  <?php the_field('ending'); ?>
</section>