<?php /*
HISTORY DESCRIPTION AND 3 POINT MISSION
*/ ?>

<section class="history full-width">

	<div class="section-header full-width">
		<h2>Our <b>History</b></h2>
	</div>

	<div class="history-contents">
		<div class="one-half history-image" style="background-image: url(<?php the_field('history_image'); ?>)"></div>
		<div class="one-half history-text"><?php the_field('history_text'); ?></div>
		<div style="clear: both"></div>
	</div>

</section>


<section class="mission max-width">
	<?php if( have_rows('mission_point') ) {
		$i = 0;
    while ( have_rows('mission_point') ) : $i++; the_row(); ?>

    	<div class="mission-point one-half">
        <?php if ($i == 1) { ?>
          <h2>Our <b>Vision</b></h2>
        <?php } else { ?>
          <h2>Our <b>Mission</b></h2>
        <?php } ?>
    		<p><?php the_sub_field('point_content'); ?></p>
    	</div>

    <?php endwhile;
  } else { ?>
    <!--no content -->>
  <?php } ?>
  <div style="clear: both"></div>
</section>


<section class="values">
  <div class="max-width">
    <h2>Our <b>Core Values</b></h2>
    <?php if( have_rows('core_value') ) {
      while ( have_rows('core_value') ) : the_row(); ?>

        <div class="value-tile one-third">
          <div class="above-overlay">
            <?php $image = get_sub_field('value_icon'); ?>
            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
            <h4><?php the_sub_field('value_title'); ?></h4>
            <p><?php the_sub_field('value_description'); ?></p>
          </div>
          <div class="background-img" style="background-image: url('<?php the_sub_field('value_background'); ?>')" /></div>
          <div class="dark-overlay"></div>
        </div>

      <?php endwhile;
    } else { ?>
      <!--no content -->>
    <?php } ?>
    <div style="clear: both"></div>
  </div>
</section>
