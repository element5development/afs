<?php /*
DISPLAY LANDING PAGE FORM
*/ ?>

<div id="landing-form-link"></div>
<section class="landing-form full-width">

    <div class="mission-point max-width">
  		<h2><?php the_field('landing_form_header'); ?></h2>
  		<?php the_field('landing_form') ?>
    </div>
		
</section>