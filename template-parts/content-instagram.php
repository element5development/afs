<?php /*
DISPLAY INSTAGRAM FEED AND LINK
*/ ?>

<?php if ( is_page(53) ) { ?>
  <section class="instagram full-width">
  	<h2>#afscareers4u</h2>
    <?php echo wdi_feed(array('id'=>'4')); ?>
    <a target="_blank" class="primary-button arrow" href="https://www.instagram.com/appliedfitnesssolutions/">Follow Us</a>
  </section>
<?php } elseif ( is_page(11150) ) { ?>
  <section class="instagram full-width">
    <h2>#AFSplymouth</h2>
    <?php echo wdi_feed(array('id'=>'2')); ?>
    <a target="_blank" class="primary-button arrow" href="https://www.instagram.com/appliedfitnesssolutions/">Follow Us</a>
  </section>
<?php } elseif ( is_page(11151) ) { ?>
  <section class="instagram full-width">
    <h2>#AFSAnnArbor</h2>
    <?php echo wdi_feed(array('id'=>'1')); ?>
    <a target="_blank" class="primary-button arrow" href="https://www.instagram.com/appliedfitnesssolutions/">Follow Us</a>
  </section>
<?php } elseif ( is_page(11152) ) { ?>
  <section class="instagram full-width">
    <h2>#AFSRochester</h2>
    <?php echo wdi_feed(array('id'=>'3')); ?>
    <a target="_blank" class="primary-button arrow" href="https://www.instagram.com/appliedfitnesssolutions/">Follow Us</a>
  </section>
<?php } else { 
  //NOTHING 
} ?>