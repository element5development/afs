<?php /*
SECONDARY NAVIGATION
*/ ?>

<?php if( $post->post_parent == '229' && !is_page('10995') ) { ?>
  <section class="secondary-nav full-width">
    <nav><?php wp_nav_menu( array( 'theme_location' => 'weight-loss-menu' ) ); ?></nav>
  </section>
<?php } ?>