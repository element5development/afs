<?php /*
CHILD PROGRAM NAVIGATION
*/ ?>


<section class="program-nav full-width">
	<div class="max-width">
    
    <?php $i = 0; ?>
    <?php if( have_rows('program_navigation') ) {
      while ( have_rows('program_navigation') ) : $i++; the_row(); ?>

        <a href="#program-<?php echo $i ?>" class="program-nav-link smoothScroll" style="background-image: url(<?php the_sub_field('link_image')  ?>);">
          <div class="vertical-center-parent">
            <div class="above-overlay"><?php the_sub_field('link_title')  ?></div>
            <div class="dark-overlay"></div>
          </div>
        </a>

      <?php endwhile;
    } else { ?>
      <!--no content -->>
    <?php } ?>

  </div>
</section>