<?php /*
DIGITAL CONSULTATION FORM
*/ ?>

<?php
	$result = $_GET["result"];
	if ( $result == 'wl') { 
		$solution = 'Fitness Solution';
	} elseif ( $result == "pc" ) { 
		$solution = 'Fitness Coaching';
	} elseif ( $result == 'ss') {
		$solution = 'Strength Solution';
	} else {
		$solution = 'Fitness Solution';
	}
?>

<section class="digital-results-container">
	<div class="digital-results-header section-header full-width">
		<h2>You scored:</h2>
	</div>

	<div class="max-width">
		<div class="weight-loss digital-results">
			<h1><?php echo $solution; ?></h1>

			<?php if ( $result == 'wl') { ?>

			<p>Your score lines up with our fitness solution program. Did you know within an hour after your first workout
				mood enhancing chemicals like serotonin, dopamine, and norepinephrine flood your brain? Whether your goal
				involves weight loss/toning, conditioning, or just feeling better, this might be the perfect fit for you! Want
				to make sure?</p>
			<p>Get more clarity on how AFS can help you without leaving your home or office! Use our scheduler tool below
				and one of our trusted (human) team members will learn more about your specific needs and give further
				recommendations in a convenient one on one online conference.</p>

			<?php } elseif ( $result == "pc" ) { ?>

			<p>You scored high for needing some guidance. Don't worry, you're not alone! A 2012 study by Harris
				Interactive found that over 70% of those who made new years resolutions involving fitness goals ended up
				giving up before reaching them. Why?</p>
			<ul>
				<li>42 percent say it's too difficult to follow a diet or workout regimen</li>
				<li>38 percent say it's too hard to get back on track once they fall off</li>
				<li>36 percent say it's hard to find time</li>
			</ul>
			<p>Without a plan, some education, and a generous serving of accountability, getting to your goal can be
				an uphill battle. Lucky for you, our memberships are built on support! Want to make sure this fits you?</p>
			<p>Get more clarity on how AFS can help you without leaving your home or office! Use our scheduler tool below and
				one of our trusted (human) team members will learn more about your specific needs and give further
				recommendations in a convenient one on one online conference.</p>

			<?php } elseif ( $result == 'ss') { ?>

			<p>Looks like your current priorities match up with our strength solution program! There is no one size fits all
				strength training plan, so we're going to need to learn more of your story to fully endorse this digital result.
				Do you have any injuries you're working through? What's your experience with resistance training, etc.</p>
			<p>Use our scheduler tool below and one of our trusted (human) team members will learn more about your specific
				needs and give further recommendations in a convenient one on one online conference.</p>

			<?php } else { ?>

			<p>Your score lines up with our fitness solution program. Did you know within an hour after your first workout
				mood enhancing chemicals like serotonin, dopamine, and norepinephrine flood your brain? Whether your goal
				involves weight loss/toning, conditioning, or just feeling better, this might be the perfect fit for you! Want
				to make sure?<p>
					<p>Get more clarity on how AFS can help you without leaving your home or office! Use our scheduler tool below
						and one of our trusted (human) team members will learn more about your specific needs and give further
						recommendations in a convenient one on one online conference.</p>

					<?php }	?>

					<h4>Book a Discovery Meeting</h4>

					<!-- Calendly inline widget begin -->
					<div class="calendly-inline-widget" data-url="https://calendly.com/andrew-afs" style="min-width:320px;height:630px;"></div>
					<script type="text/javascript" src="https://assets.calendly.com/assets/external/widget.js"></script>
					<!-- Calendly inline widget end -->

					<!-- <?php $visitorcity = do_shortcode('[geoip-city]'); ?>
			<?php if ( $visitorcity == 'Ann Arbor' || $visitorcity == 'Ypsilanti' || $visitorcity == 'Ypsilanti Charter Twp' || $visitorcity == 'Saline' || $visitorcity == 'Pittsfield Charter Twp' ) { ?>
			<a href="tel:+1(734)994-8570" class="primary-button">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-phone-white.png" />
				+1 (734) 994-8570
			</a>
			<?php } elseif ( $visitorcity == 'Plymouth' || $visitorcity == 'Livonia' || $visitorcity == 'Canton' || $visitorcity == 'Westland' || $visitorcity == 'Plymouth Charter Twp' || $visitorcity == 'Northville' ) { ?>
			<a href="tel:+1(734)357-8920" class="primary-button">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-phone-white.png" />
				+1 (734) 357-8920
			</a>
			<?php } elseif ( $visitorcity == 'Rochester' || $visitorcity == 'Rochester Hills' || $visitorcity == 'Auburn Hills' || $visitorcity == 'Troy' || $visitorcity == 'Utica' || $visitorcity == 'Shelby Charter Township' ) { ?>
			<a href="tel:+1(877)423-7348" class="primary-button">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-phone-white.png" />
				877-4AFS-FIT
			</a>
			<?php } else { ?>
			<a href="tel:+1(877)423-7348" class="primary-button">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-phone-white.png" />
				877-4AFS-FIT
			</a>
			<?php } ?>

			<?php if ( $result == 'wl') { ?>
			<a href="/our-classes/fitness-solution/">
				<h3>What’s in the fitness program?</h3>
			</a>
			<?php } elseif ( $result == "pc" ) { ?>
			<a href="/about-us/fitness-coaching/">
				<h3>What’s in the personal coaching program?</h3>
			</a>
			<?php } elseif ( $result == 'ss') { ?>
			<a href="/our-classes/strength-solution/">
				<h3>What’s in the strength solution program?</h3>
			</a>
			<?php } else { ?>
			<a href="/our-classes/fitness-solution/">
				<h3>What’s in the fitness program?</h3>
			</a>
			<?php }	?>
		</div>
		<a href="/digital-consultation/" class="primary-button arrow back">Retake this</a> -->
		</div>
</section>