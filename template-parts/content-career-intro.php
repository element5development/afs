<?php /*
INTRO CONTENT FOR CAREERS PAGE
*/ ?>

<section class="career-intro full-width">
	<div class="max-width">

		<div class="orange-bubble-text">AFS is an exciting place to work.</div>
    <div class="career-intro-entrance hidden">
  		<div class="line-one">We’re a fast-growing company with</div>
  		<div class="line-two">three locations:</div>
  		<div class="line-three">Ann Arbor, Plymouth and Rochester.</div>
  		<div class="line-four">We only hire the most passionate individuals,</div>
  		<div class="line-five">so you’ll be working alongside the most dedicated team in the industry—</div>
  		<div class="line-six">If you thrive on making the world better for those around you,</div>
      <div class="line-seven">we want to hear from you.</div>
    </div>

	</div>
</section>
