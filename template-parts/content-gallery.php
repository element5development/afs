<?php /*
FULLSCREEN 6 COLUMN BASIC GALLERY
*/ ?>

<?php if ( is_page(10994) || is_page(10992) ) { ?>

  <section class="gallery full-width">
    <div class="darkoverlay left"></div>
    <?php $images = get_field('gallery');
    if( $images ) { ?>
      <div id="slider" class="flexslider">
        <ul class="slides">
          <?php foreach( $images as $image ): ?>
            <li>
              <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
            </li>
          <?php endforeach; ?>
        </ul>
      </div>
    <?php } ?>
    <div class="darkoverlay right"></div>
  </section>

<?php } else { ?>

  <section class="gallery full-width">
    <?php
    $images = get_field( 'gallery' );
    if( $images ) { ?>
      <?php foreach( $images as $image ): ?>
        <?php $image_large = wp_get_attachment_image_src($image['id'], 'large'); ?>
        <?php $image_thb = wp_get_attachment_image_src($image['id'], 'gallery-thumbnail'); ?>
        <?php $image_caption = get_post( $image['id'] ); ?>
        <?php $image_alt = get_post_meta( $image['id'], '_wp_attachment_image_alt', true); ?>
        <a title="<?php echo $image_caption->post_excerpt; ?>" href="<?php echo $image_large[0]; ?>" rel="lightbox[gallery]">
          <img src="<?php echo $image_thb[0]; ?>" class="attachment-thumbnail" alt="<?php echo $image_alt; ?>" />
          <div class="gallery-overlay"></div>
        </a>
      <?php endforeach; ?>
    <?php } ?>
  </section>

<?php } ?>