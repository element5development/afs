<?php /*
PROGRAM PREVIEW CLICK-TO-ACTIONS
*/ ?>


<section class="program-ctas full-width">
	<div class="max-width">
    <?php $i = 0; ?>
    <?php if( have_rows('program') ) {
      while ( have_rows('program') ) : $i++; the_row(); ?>

        <?php if ( is_page(37) ) { ?>
          <div class="program-container">
            <div class="one-half program-title">
              <?php $image = get_sub_field('program_image'); ?>
              <img class="program-img" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/>
              <h2><?php the_sub_field('program_title'); ?></h2>
            </div>
            <div class="one-half program-contents">
              <?php the_sub_field('program_contents'); ?>
            </div>
            <div style="clear: both"></div>
          </div>
        <?php } else { ?>
					<div id="program-<?php echo $i; ?>" class="progam-anchor"></div>
          <div class="program-container hidden">
            <div class="one-half program-title">
              <?php $image = get_sub_field('program_image'); ?>
              <img class="program-img" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
            </div>
            <div class="one-half program-contents">
              <h2><?php the_sub_field('program_title'); ?></h2>
              <?php the_sub_field('program_contents'); ?>
            </div>
            <div style="clear: both"></div>
          </div>
        <?php } ?>

      <?php endwhile;
    } else { ?>
      <!--no content -->>
    <?php } ?>
  </div>
</section>

<?php if ( is_page(37) ) { ?>
  <div class="numbers-never-lie">
  	<img src="<?php bloginfo('stylesheet_directory'); ?>/img/numbers-never-lie.png" alt="numbers never lie chart"/>
    <?php the_field('numbers_never_lie'); ?>
  </div>
<?php } ?>