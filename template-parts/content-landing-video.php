<?php /*
DISPLAY LANDING PAGE VIDEO AND KEY POINTS
*/ ?>

<section class="landing-video full-width">
	<div class="max-width">

    <h2><?php the_field('landing_video_header'); ?></h2>
		<div class="two-third">
      <?php the_field('landing_video'); ?>
    </div>
    <div class="one-third">
      <?php the_field('landing_key_points'); ?>
    </div>
    <div style="clear: both"></div>
		
	</div>
</section>