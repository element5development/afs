<?php /*
FAQ COMPARISON OF MEMBERSHIPS
*/ ?>

<section class="afs-difference afs-faq">

	<div class="above-overlay">
		<h2>Our <b>FAQ</b></h2>
		<div class="list-container">
			<div class="one-half">
				<h3>What do your programs include?</h3>
				<?php the_field('your_programs'); ?>
			</div>
			<div class="one-half">
				<h3>Memberships</h3>
				<?php the_field('afs_memberships'); ?>
			</div>
			<div style="clear: both"></div>
		</div>
	</div>
	<div class="white-overlay"></div>

</section>