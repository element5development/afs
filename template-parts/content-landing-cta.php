<?php /*
DISPLAY LANDING PAGE VIDEO AND KEY POINTS
*/ ?>

<section class="landing-cta full-width">
	<div class="max-width">

    <?php the_field('landing_cta'); ?>

    <?php 
      $visitorcity = do_shortcode('[geoip-city]'); 
      if ( $visitorcity == 'Ann Arbor' || $visitorcity == 'Ypsilanti' || $visitorcity == 'Ypsilanti Charter Twp' || $visitorcity == 'Saline' || $visitorcity == 'Pittsfield Charter Twp' ) { ?>
        <a href="tel:+1(734)994-8570" class="primary-button arrow">
          Schedule an Appointment
        </a>
      <?php } elseif ( $visitorcity == 'Plymouth' || $visitorcity == 'Livonia' || $visitorcity == 'Canton' || $visitorcity == 'Westland' || $visitorcity == 'Plymouth Charter Twp' || $visitorcity == 'Northville' ) { ?>
        <a href="tel:+1(734)357-8920" class="primary-button arrow">
          Schedule an Appointment
        </a>
      <?php } elseif ( $visitorcity == 'Rochester' || $visitorcity == 'Rochester Hills' || $visitorcity == 'Auburn Hills' || $visitorcity == 'Troy' || $visitorcity == 'Utica' || $visitorcity == 'Shelby Charter Township' ) { ?> 
        <!-- <a href="tel:+1(734)994-8570" class="primary-button arrow">
          Schedule an Appointment
        </a> -->
        <a href="tel:+1(877)423-7348" class="primary-button arrow">
          Schedule an Appointment
        </a>
      <?php } else { ?>
        <a href="tel:+1(877)423-7348" class="primary-button arrow">
          Schedule an Appointment
        </a>
      <?php } 
    ?> 
  
		
	</div>
</section>