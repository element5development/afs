<?php /*
SUCCESS STORIES PREVIEW FORMATTING
*/ ?>

<section class="blog-preview full-width">

	<h2><strong>Latest from</strong> our online community</h2>

	<div class="featured-post-preview">

		<?php $mainimage = get_field('main_image'); ?>
		<article class="featured-blog-post full-width" style="background-image: url(<?php echo $mainimage['url']; ?>);">
			<div class="above-overlay max-width">
				<h3><?php the_field('main_title'); ?></h3>
				<p><?php the_field('main_description'); ?></p>
				<?php $mainlink = get_field('main_link'); ?>
				<a href="<?php echo $mainlink['url']; ?>" class="secondary-button arrow"><?php echo $mainlink['title']; ?></a>
			</div>
			<div class="dark-overlay"></div>
		</article>

	</div>
	<div class="max-width">
		<div class="latest-post-preview">

			<?php if( have_rows('post_repeater') ): ?>

			<?php while ( have_rows('post_repeater') ) : the_row(); ?>

			<?php $image = get_sub_field('post_image'); ?>
			<?php $link = get_sub_field('post_link'); ?>

			<a href="<?php echo $link['url']; ?>">
				<article class="blog-post-preview one-third">
					<div class="image-cropped background-check" style="background-image: url(<?php echo $image['url']; ?>);">
					</div>
					<div class="post-info">
						<div class="post-date"><?php the_sub_field('post_date'); ?></div>
						<div style="clear: both"></div>
					</div>
					<h3><?php the_sub_field('post_title'); ?></h3>
				</article>
			</a>

			<?php endwhile; ?>

			<?php endif; ?>

			<div style="clear: both"></div>

		</div>
	</div>

</section>