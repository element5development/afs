<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying 2 column system with price card

\*----------------------------------------------------------------*/
?>

<section class="price-card-2-column <?php the_sub_field('width'); ?>">
	<div>
		<div class="price-card hidden visible animated zoomIn">
			<h3><?php the_sub_field('card_title'); ?></h3>
			<h4 class="price-container">
				<span class="price">$<?php the_sub_field('price'); ?><sup>00</sup></span>
				<span class="label"><?php the_sub_field('price_label'); ?></span>
			</h4>
			<div class="option-contents">
				<?php the_sub_field('card_description'); ?>
			</div>
		</div>
	</div>
	<div>
		<?php the_sub_field('content-right'); ?>
	</div>
</section>