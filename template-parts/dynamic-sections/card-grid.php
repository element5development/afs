<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying grid of cards

\*----------------------------------------------------------------*/
?>

<?php
	$columns = get_sub_field('columns');
?>

<section class="card-grid <?php the_sub_field('width'); ?> columns-<?php echo $columns; ?>">
	<?php while ( have_rows('cards') ) : the_row(); ?>
		<div class="card">
			<!-- IMAGE -->
			<?php $image = get_sub_field('image'); ?>
			<?php if ( get_sub_field('image') ) : ?>
				<figure>
					<img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
				</figure>
			<?php endif; ?>
			<!-- HEADLINE -->
			<?php if ( get_sub_field('title') ) : ?>
				<h3><?php the_sub_field('title') ?></h3>
			<?php endif; ?>
			<!-- DESCRIPTION -->	
			<?php if ( get_sub_field('description') ) : ?>
				<p><?php the_sub_field('description'); ?></p>
			<?php endif; ?>
			<!-- BUTTON -->
			<?php
				$link = get_sub_field('button'); 
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self'; 
				if ( get_sub_field('button') ) : 
			?>
				<a class="primary-button" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
					<?php echo esc_html($link_title); ?>
				</a>
			<?php endif; ?>
		</div>
	<?php endwhile; ?>
</section>