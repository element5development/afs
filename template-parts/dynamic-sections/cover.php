<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying content with background image

\*----------------------------------------------------------------*/
?>
<?php $image = get_sub_field('background'); ?>
<section class="cover <?php the_sub_field('width'); ?> <?php if( !$image ) :?>has-no-image<?php endif; ?>" style="background-image:url(<?php echo $image['url']; ?>);">
	<div>
		<?php the_sub_field('content'); ?>
	</div>
</section>