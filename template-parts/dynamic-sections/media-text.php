<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying 2 columns one with a image the other with an editor

\*----------------------------------------------------------------*/
?>

<section class="media-text <?php the_sub_field('width'); ?> <?php the_sub_field('image_alignment'); ?>">
	<div>
		<?php $image = get_sub_field('image'); ?>
		<img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
	</div>
	<div>
		<?php the_sub_field('content'); ?>
	</div>
</section>