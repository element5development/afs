<?php /*
PAGE TITLES AND BACKGROUND IMAGE
*/ ?>

<!--URL for featured image-->
<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' ); ?>



<?php if ( is_front_page() ) { ?>
<!-- HOME PAGE -->
	<?php if ( $src[0] == "") {
		$featuredimg = "/wp-content/themes/afs/img/default-page-header.jpg";
	} else {
		$featuredimg = $src[0];
	}	?>
	<div class="home-page-top page-top" 
		<?php if ( get_field('background_video_mp4') ) { } else { ?> 
			style="background-image: url(<?php echo $featuredimg; ?>);" 
		<?php } ?> 
	> 
		<div class="above-overlay">
			<h3 style="text-align: center;"><strong>Work out in a trusted group setting</strong></h3>
			<?php the_field('header_text'); ?>
		</div>
		<div class="dark-overlay"> 
			<?php if ( get_field('background_video_mp4') ) { ?>
				<video muted="" autoplay="" loop="" poster="<?php echo $src[0]; ?>" class="bgvid"> 
					<source src="<?php the_field('background_video_mp4'); ?>" type="video/mp4"> 
					<source src="<?php the_field('background_video_webm'); ?>" type="video/webm"> 
				</video> 
			<?php } ?>
		</div> 
		<!-- BREADCRUMBS -->
		<?php if ( function_exists('yoast_breadcrumb') && !is_front_page() ) { ?>
			<div class="breadcrumbs full-width">
				<div class="max-width">
					<?php yoast_breadcrumb(); ?>
					<div style="clear: both"></div>
				</div>
			</div>
		<?php } ?>
	</div>
<!-- END HOME PAGE -->
<?php } elseif ( is_page_template( 'template-landing.php' ) ) { ?>
<!-- LANDING PAGE -->
	<?php if ( $src[0] == "") {
		$featuredimg = "/wp-content/themes/afs/img/default-page-header.jpg";
	} else {
		$featuredimg = $src[0];
	}	?>
	<div class="home-page-top landing-page-top page-top" style="background-image: url(<?php echo $featuredimg; ?>);"> 
		<div class="landing-header">
			<img id="nav-logo" src="<?php bloginfo('stylesheet_directory'); ?>/img/afs-landing-logo.svg" alt="Applied Fitness Solutions" />
			<?php 
        $visitorcity = do_shortcode('[geoip-city]'); 
        if ( $visitorcity == 'Ann Arbor' || $visitorcity == 'Ypsilanti' || $visitorcity == 'Ypsilanti Charter Twp' || $visitorcity == 'Saline' || $visitorcity == 'Pittsfield Charter Twp' ) { ?>
        	<a href="tel:+1(734)994-8570" class="secondary-button">
						+1 (734) 994-8570
					</a>
        <?php } elseif ( $visitorcity == 'Plymouth' || $visitorcity == 'Livonia' || $visitorcity == 'Canton' || $visitorcity == 'Westland' || $visitorcity == 'Plymouth Charter Twp' || $visitorcity == 'Northville' ) { ?>
        	<a href="tel:+1(734)357-8920" class="secondary-button">
						+1 (734) 357-8920
					</a>
        <?php } elseif ( $visitorcity == 'Rochester' || $visitorcity == 'Rochester Hills' || $visitorcity == 'Auburn Hills' || $visitorcity == 'Troy' || $visitorcity == 'Utica' || $visitorcity == 'Shelby Charter Township' ) { ?> 
        	<a href="tel:+1(248)923-1030" class="secondary-button">
						+1 (248) 923-1030
					</a>
        <?php } else { ?>
        	<a href="tel:+1(877)423-7348" class="secondary-button">
						877-4AFS-FIT
					</a>
        <?php } 
	    ?> 
		</div>
		<div class="above-overlay">
			<?php if ( is_page(11522) ) { ?>
				<h3 id="arc-text" style="text-align: center;"><strong>Work out in a trusted group setting</strong></h3>
			<?php } ?>
			<?php the_field('header_text'); ?>
		</div>
		<div class="dark-overlay"></div> 
	</div>
<!-- END LANDING PAGE -->
<?php } elseif ( is_archive() || is_home() ) { ?>
<!-- BLOG / ARCHIVES / CATEGORIES -->
<div class="featured-post-preview page-top">

	<!-- QUERY LATEST FEATURED BLOG POST THAT IS NOT A SUCCESS STORY -->
	<?php 
	  $args = array(
	    'posts_per_page' => 1,
			'meta_query' => array(
				array(
					'key' => 'featured_post',
					'value' => '1',
					'compare' => '=='
				)
			)
	  );
	  $query = new WP_Query( $args );
	?>
	<?php  if ( $query->have_posts() ) { ?>
	  <?php while ($query->have_posts()) : $query->the_post(); ?>
	  	<?php 
				//USE FEATURED IMAGE OTHERWISE USE DEFAULT IAMGE
				$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' ); 
				if ( $src[0] == "") {
					$featuredimg = "/wp-content/themes/afs/img/default-blog-img.jpg";
				} else {
					$featuredimg = $src[0];
				}	
			?>
	  	<article class="featured-blog-post full-width" style="background-image: url(<?php echo $featuredimg; ?>);">
	  		<div class="above-overlay max-width">
	  			<h3><?php the_title(); ?></h3>
	  			<p><?php the_excerpt(); ?></p>
	  			<a href="<?php the_permalink(); ?>" class="secondary-button arrow">Read the Rest</a>
	  		</div>
	  		<div class="dark-overlay"></div>
	  	</article>
	  <?php endwhile; ?>
	<?php } ?>

	<!--BREADCRUMBS-->
	<?php if ( function_exists('yoast_breadcrumb') && !is_front_page() ) { ?>
			<div class="breadcrumbs full-width">
				<div class="max-width">
					<?php yoast_breadcrumb(); ?>
					<div style="clear: both"></div>
				</div>
			</div>
		<?php } ?>
	</section>
</div>

<!-- END BLOG / ARCHIVES / CATEGORIES -->
<?php } elseif ( is_search() ) { ?>
<!-- SEARCH RESULTS -->

	<?php $featuredimg = "/wp-content/themes/afs/img/default-page-header.jpg"; ?>
	<?php $search_query = get_search_query(); ?>
	<section class="page-top" style="background-image: url(<?php echo $featuredimg; ?>);">
		<div class="above-overlay">
			<h1>Search Results for "<?php echo $search_query ?>"</h1>
		</div>
		<div class="dark-overlay"></div>
		<!-- BREADCRUMBS -->
		<?php if ( function_exists('yoast_breadcrumb') && !is_front_page() ) { ?>
			<div class="breadcrumbs full-width">
				<div class="max-width">
					<?php yoast_breadcrumb(); ?>
					<div style="clear: both"></div>
				</div>
			</div>
		<?php } ?>
	</section>

<!-- END SEARCH RESULTS-->
<?php } elseif ( is_404() ) { ?>
<!-- 404 ERROR -->

	<?php $featuredimg = "/wp-content/themes/afs/img/default-page-header.jpg"; ?>
	<section class="page-top" style="background-image: url(<?php echo $featuredimg; ?>);">
		<div class="above-overlay">
			<h1>Lost Page</h1>
		</div>
		<div class="dark-overlay"></div>
		<!-- BREADCRUMBS -->
		<?php if ( function_exists('yoast_breadcrumb') && !is_front_page() ) { ?>
			<div class="breadcrumbs full-width">
				<div class="max-width">
					<?php yoast_breadcrumb(); ?>
					<div style="clear: both"></div>
				</div>
			</div>
		<?php } ?>
	</section>

<!-- END 404 ERROR -->
<?php } elseif ( is_singular( 'team' ) ) { ?>
	<!-- TEAM MEMBER -->

	<?php $featuredimg = "/wp-content/themes/afs/img/default-trainer-header.jpg"; ?>
	<section class="page-top" style="background-image: url(<?php echo $featuredimg; ?>);">
		<div class="above-overlay">
			<h1><?php the_title(); ?></h1>
		</div>
		<div class="dark-overlay"></div>
		<!-- BREADCRUMBS -->
		<?php if ( function_exists('yoast_breadcrumb') && !is_front_page() ) { ?>
			<div class="breadcrumbs full-width">
				<div class="max-width">
					<?php yoast_breadcrumb(); ?>
					<div style="clear: both"></div>
				</div>
			</div>
		<?php } ?>
	</section>

	<!-- END TEAM MEMBER -->
<?php } elseif ( is_single() ) { ?>
<!-- SINGLE POST -->
	<?php if ( in_category('success-stories') )  {
		$sex = get_field('male_or_female');
    if ( get_field('custom_header') ) {
      $featuredimg = get_field('custom_header');
    } elseif ( $sex == 'male' ) {
			$featuredimg = "/wp-content/themes/afs/img/success-story-male.jpg";
		} else {
			$featuredimg = "/wp-content/themes/afs/img/success-story-female.jpg";
		}
	} elseif ( $src[0] == "") {
		$featuredimg = "/wp-content/themes/afs/img/default-blog-img.jpg";
	} else {
		$featuredimg = $src[0];
	} ?>
	<?php if ( in_category('success-stories') ) { ?>
		<section class="page-top" style="background-image: url(<?php echo $featuredimg; ?>);">
			<div class="above-overlay">
				<h1><?php the_title(); ?></h1>
				<!-- VIDEO -->
				<?php if ( get_field('video_iframe_link') ) { ?>
					<a class="fancybox fancybox.iframe" href="<?php the_field('video_iframe_link'); ?>" ata-fancybox-type="iframe">Watch Story</a>
				<?php } ?>
			</div>
			<div class="dark-overlay"></div>
			<!-- BREADCRUMBS -->
			<?php if ( function_exists('yoast_breadcrumb') && !is_front_page() ) { ?>
				<div class="breadcrumbs full-width">
					<div class="max-width">
						<?php yoast_breadcrumb(); ?>
						<div style="clear: both"></div>
					</div>
				</div>
			<?php } ?>
		</section>
	<?php } else { ?>
		<section class="page-top" style="background-image: url(<?php echo $featuredimg; ?>);">
			<div class="above-overlay">
				<h1><?php the_title(); ?></h1>
			</div>
			<div class="dark-overlay"></div>
			<!-- BREADCRUMBS -->
			<?php if ( function_exists('yoast_breadcrumb') && !is_front_page() ) { ?>
				<div class="breadcrumbs full-width">
					<div class="max-width">
						<?php yoast_breadcrumb(); ?>
						<div style="clear: both"></div>
					</div>
				</div>
			<?php } ?>
		</section>
	<?php } ?>

<!-- END SINGLE POST -->
<?php } else { ?>
<!-- PAGE -->
	<?php if ( $src[0] == "") {
		$featuredimg = "/wp-content/themes/afs/img/default-page-header.jpg";
	} else {
		$featuredimg = $src[0];
	}	?>
		<?php if ( get_field('background_video_mp4') ) { ?>
			<div class="videowrap page-top">
		<?php } else { ?> 
			<div class="page-top" style="background-image: url(<?php echo $featuredimg; ?>);" >
		<?php } ?> 
		<div class="above-overlay">
			<?php if ( get_field('background_video_mp4') ) { 
				the_field('header_text'); 
			} else {
				the_title( '<h1>', '</h1>' );
			} ?>

		</div>
		<div class="dark-overlay"> 
			<?php if ( get_field('background_video_mp4') ) { ?>
				<video muted="" autoplay="" loop="" poster="" class="bgvid"> 
					<source src="<?php the_field('background_video_mp4'); ?>" type="video/mp4"> 
					<source src="<?php the_field('background_video_webm'); ?>" type="video/webm"> 
				</video> 
			<?php } ?>
		</div> 
		<!-- BREADCRUMBS -->
		<?php if ( function_exists('yoast_breadcrumb') && !is_front_page() ) { ?>
			<div class="breadcrumbs full-width">
				<div class="max-width">
					<?php yoast_breadcrumb(); ?>
					<div style="clear: both"></div>
				</div>
			</div>
		<?php } ?>
	</div>
<!-- END PAGE -->
<?php } ?>