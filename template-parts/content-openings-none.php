<?php /*
SORTABLE TABLE OF JOB OPENINGS
*/ ?>

<section class="openings no-openings full-width">
  <div class="max-width">

    <h2>No Current <b>Openings</b></h2>
    <?php the_field('no_openings'); ?>
    
  </div>
</section>