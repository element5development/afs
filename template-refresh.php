<?php /*
Template Name: refresh
*/ ?>

<?php 
  $args = array(
    'orderby' => 'rand',
    'order' => 'ASC',
    'posts_per_page' => 3,
    'category_name' => 'success-stories'
  );
  $query = new WP_Query( $args );
?>
  
<?php  if ( $query->have_posts() ) { ?>
  <?php while ($query->have_posts()) : $query->the_post(); ?>

  <a href="<?php the_permalink(); ?>">
    <article class="sucess-story-preview one-third">
      <div class="name-pic">
        <img src="<?php the_field( 'headshot' ) ?>" />
        <h4><?php the_field( 'name' ) ?></h4>
      </div>
      <div class="quote">
        <p><?php the_field( 'testimony' ) ?></p>
        <a class="full-story" href="<?php the_permalink(); ?>">read full story</a>
      </div>
      <div style="clear: both"></div>
    </article>
  </a>

  <?php endwhile; ?>
<?php } ?>