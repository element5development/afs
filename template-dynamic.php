<?php /*
Template Name: Dynamic
*/ ?>

<?php get_header(); ?>

<!--URL for featured image-->
<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' ); ?>

<main class="full-width">
	<!-- PAGE TITLE -->
		<?php if ( $src[0] == "") {
			$featuredimg = "/wp-content/themes/afs/img/default-page-header.jpg";
		} else {
			$featuredimg = $src[0];
		}	?>
		<?php if ( get_field('background_video_mp4') ) { ?>
			<div class="videowrap page-top">
		<?php } else { ?> 
			<div class="page-top" style="background-image: url(<?php echo $featuredimg; ?>);" >
		<?php } ?> 
			<div class="above-overlay">
				<?php if ( get_field('background_video_mp4') ) { 
					the_field('header_text'); 
				} else {
					the_title( '<h1>', '</h1>' );
				} ?>
			</div>
			<div class="dark-overlay"> 
				<?php if ( get_field('background_video_mp4') ) { ?>
					<video muted="" autoplay="" loop="" poster="" class="bgvid"> 
						<source src="<?php the_field('background_video_mp4'); ?>" type="video/mp4"> 
						<source src="<?php the_field('background_video_webm'); ?>" type="video/webm"> 
					</video> 
				<?php } ?>
			</div> 
			<!-- BREADCRUMBS -->
			<?php if ( function_exists('yoast_breadcrumb') && !is_front_page() ) { ?>
				<div class="breadcrumbs full-width">
					<div class="max-width">
						<?php yoast_breadcrumb(); ?>
						<div style="clear: both"></div>
					</div>
				</div>
			<?php } ?>
		</div>
	<!-- DYNAMIC ACF ARTICLE -->
	<?php if( have_rows('article') ):  ?>
		<article>
			<?php 
			/*----------------------------------------------------------------*\
			|
			| Insert page content which is most often handled via ACF Pro
			| and highly recommend the use of the flexiable content so
			|	we already placed that code here.
			|
			| https://www.advancedcustomfields.com/resources/flexible-content/
			|
			\*----------------------------------------------------------------*/
			?>
			<?php
				while ( have_rows('article') ) : the_row();
					if( get_row_layout() == 'editor' ):
						get_template_part( 'template-parts/dynamic-sections/editor');
					elseif( get_row_layout() == '2editor' ):
						get_template_part( 'template-parts/dynamic-sections/editor-2-column');
					elseif( get_row_layout() == '3editor' ):
						get_template_part( 'template-parts/dynamic-sections/editor-3-column');
					elseif( get_row_layout() == 'media+text' ):
						get_template_part( 'template-parts/dynamic-sections/media-text');
					elseif( get_row_layout() == 'cover' ):
						get_template_part( 'template-parts/dynamic-sections/cover');
					elseif( get_row_layout() == 'pricecard' ):
						get_template_part( 'template-parts/dynamic-sections/price-card');
					elseif( get_row_layout() == 'card_grid' ):
						get_template_part( 'template-parts/dynamic-sections/card-grid');
					endif;
				endwhile;
			?>
			<?php if ( !empty( get_the_content() ) ) : ?>
				<section class="is-standard">
					<?php the_content(); ?>
				</section>
			<?php endif; ?>
		</article>
	<?php endif; ?>

</main>

<?php get_footer(); ?>