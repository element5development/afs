<?php /*
ARCHIEVE & CATEGORY PAGE FOR DEFAULT POST TYPE
*/ ?>

<?php get_header(); ?>

<main class="full-width">

	<!-- PAGE TITLES -->
	<?php get_template_part( 'template-parts/content', 'page-top' ); ?>

	<div class="max-width clearfix">

		<h2>Category: <?php single_cat_title(); ?></h2>

		<?php $current_category = single_cat_title("", false); ?>

		<section class="grid blog content-left clearfix">
			<?php echo do_shortcode('[ajax_load_more post_type="post" category=" ' . $current_category . ' " posts_per_page="9" scroll="false" transition_container="false" button_label="Load More"]'); ?>
		</section>

		<!--SIDEBAR-->
		<?php get_template_part( 'template-parts/content', 'right-sidebar' ); ?>

	</div>
</main>

<?php get_footer(); ?>